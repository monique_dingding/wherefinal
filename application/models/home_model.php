<?php

class Home_Model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
    }

    function sites() {
        $this->db->select('*');
        $this->db->from('zip_codes');
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function update_locations($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('locations', $data);
    }

    function get_featured_articles() {
    	$this->db->select('*');
        $this->db->from('articles');
        $this->db->where('featured', 'yes');
        $this->db->limit(3);
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_featured_establishments() {
    	$this->db->select('*');
        $this->db->from('sites');
        $this->db->where('featured', 'yes');
        $this->db->join('cities', 'sites.city_id = cities.id', 'left');
        $this->db->join('provinces', 'sites.province_id = provinces.id', 'left');
        $this->db->join('regions', 'sites.region_id = regions.id', 'left');
        $this->db->limit(3);
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_regular_establishments() {
    	$this->db->select('*');
        $this->db->from('sites');
        $this->db->join('cities', 'sites.city_id = cities.id');
        $this->db->join('provinces', 'sites.province_id = provinces.id');
        $this->db->join('regions', 'sites.region_id = regions.id');
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(8);
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_count_articles() {
    	$this->db->select('*');
        $this->db->from('articles');
        $query = $this->db->get();
        
        return count($query->result_array());
    }

    public function getArticles($page,$tag){
        $offset = 5*$page+2;
        $limit = 5;
        if(isset($tag))
            $sql = "select * from articles where tags like '%$tag%' order by created_at desc limit $offset ,$limit";
        else    
            $sql = "select * from articles order by created_at desc limit $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
    }


    public function getFeaturedArticles($page){
        $offset = 5*$page;
        $limit = 5;
        $sql = "select * from articles where featured='Yes' limit $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    public function getEstablishments($page){
        $offset = 4*$page;
        $limit = 4;
        $sql = "select sites.*,cities.city_name,provinces.province_name from sites left join cities on cities.id=sites.city_id left join provinces on provinces.id=sites.province_id limit $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
    }


    function get_recent_articles($limit = 5, $tag = '') {
    	$this->db->select('*');
        $this->db->from('articles');
        if(isset($tag))
            $this->db->like('tags', $tag);
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_article($url) {
        $this->db->select('*');
        $this->db->from('articles');
        $this->db->where('link', $url);
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query->result_array();   
    }

    function get_recent_places($limit = 5, $url = NULL, $featured  = "yes") {

        if ($featured == "no") {
            $this->db->select('*');
            $this->db->from('sites');
            // $this->db->join('sites_options', 'sites_options.site_id = sites.sites_option_id');
            // $this->db->join('options', 'options.id = sites_options.option_id');
            $this->db->join('cities', 'sites.city_id = cities.id');
            $this->db->join('provinces', 'sites.province_id = provinces.id');
            $this->db->join('regions', 'sites.region_id = regions.id');
            $this->db->limit($limit);
            $this->db->where('friendly_url !=', $url);    
            $this->db->where('featured =', "no");            
        } else {
            $this->db->select('*');
            $this->db->from('sites');
            // $this->db->join('sites_options', 'sites_options.site_id = sites.sites_option_id');
            // $this->db->join('options', 'options.id = sites_options.option_id');
            $this->db->join('cities', 'sites.city_id = cities.id');
            $this->db->join('provinces', 'sites.province_id = provinces.id');
            $this->db->join('regions', 'sites.region_id = regions.id');
            $this->db->limit($limit);
            $this->db->where('friendly_url !=', $url);    
        }

        
        $query = $this->db->get();

        return $query->result_array();   
    }

    function get_place($url) {
        $this->db->select('*');
        $this->db->from('sites');
        $this->db->join('cities', 'sites.city_id = cities.id', 'left');
        $this->db->join('provinces', 'sites.province_id = provinces.id', 'left');
        $this->db->join('regions', 'sites.region_id = regions.id', 'left');
        $this->db->where('friendly_url', $url);
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query->result_array();  
    }
    
    function get_options() {
        $this->db->select('*');
        $this->db->from('options');
        $this->db->limit(10);
        $query = $this->db->get();
        
        return $query->result_array();  
    }

    function search_est_by_option($option) {

        $option = urldecode($option);
        $this->db->select('*');
        $this->db->from('sites');
        $this->db->join('sites_options', 'sites_options.site_id = sites.id');
        $this->db->join('options', 'sites_options.option_id = options.id');
        $this->db->where('option_name', $option);
        $this->db->join('cities', 'sites.city_id = cities.id');
        $this->db->join('provinces', 'sites.province_id = provinces.id');
        $this->db->join('regions', 'sites.region_id = regions.id');
        $this->db->order_by('featured', 'desc');
        $this->db->limit(10);
        $query = $this->db->get();

        // echo "<pre>";
        // print_r($query->result_array());
        // // print_r($option);
        // exit;

        return $query->result_array();  

    }

    function get_option_by_id($option) {
        $option = urldecode($option);
        $this->db->select('*');
        $this->db->from('options');
        $this->db->where('option_name', $option);
        $query = $this->db->get();
        
        return $query->result_array();  
    }

    function create_client($newdata) {
        $newdata['cel-num'] = $newdata['cel_num'];
        $newdata['pwd'] = $newdata['password'];
        $newdata['status'] = "Pending";
        $newdata['created_at'] = date('Y-m-d H:i:s');
        unset($newdata['cel_num']);
        unset($newdata['password']);

        $this->db->insert('users', $newdata);
    }
    function get_site_settings($type) {
        $this->db->select('*');
        $this->db->from('sites_settings');
        $this->db->where('type', $type);
        $this->db->limit(1);

        $query = $this->db->get();
        
        return $query->result_array();  
    }


    function get_photosliders() {
        $this->db->select('friendly_url, cover_image, short_summary, option_name, name, sites_option_id');
        $this->db->from('photos_slider');
        $this->db->join('sites', 'photos_slider.site_id = sites.id');
        $this->db->join('sites_options', 'sites_options.site_id = sites.id');
        $this->db->join('options', 'sites_options.option_id = options.id');
        $this->db->group_by('name');
        $query = $this->db->get();
        
        return $query->result_array();  
    }


}

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="<?php echo base_url(); ?>assets/admin2/img/sidebar-5.jpg">    
    
    <!--   
        
        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" 
        Tip 2: you can also add an image using data-image tag
        
    -->
    
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="<?php echo base_url(); ?>admin/" class="simple-text">
                  <img src="<?php echo base_url(); ?>assets/admin2/img/logo.png" style="width:40%;">
                </a>
            </div>
                       
            <ul class="nav">

                <li class="<?php if($this->uri->segment(2)=="" or $this->uri->segment(2)=="index"){echo "active";}?>">
                    <a href="<?php echo base_url().$this->router->fetch_class();?>/">
                        <i class="fa fa-building-o"></i>
                        <p>Establishments</p>
                    </a>            
                </li>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="users"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/users">
                            <i class="pe-7s-users"></i>
                            <p>Users</p>
                        </a>            
                    </li>
                <?php endif;?>
                <li class="<?php if($this->uri->segment(2)=="articles"){echo "active";}?>">
                    <a href="<?php echo base_url().$this->router->fetch_class();?>/articles">
                        <i class="pe-7s-news-paper"></i> 
                        <p>Articles</p>
                    </a>        
                </li>
                <!--<li class="settings">
                    <a href="#">
                        <i class="pe-7s-news-paper"></i> 
                        <p>Site Configuration</p>
                    </a>        
                </li>-->
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="photos"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/photos">
                            <i class="pe-7s-photo"></i> 
                            <p>Photo Slider</p>
                        </a>
                    </li>
                <?php endif;?>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="options"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/options">
                            <i class="pe-7s-keypad"></i> 
                            <p>Options</p>
                        </a>
                    </li>
                <?php endif;?>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="cities"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/cities">
                            <i class="pe-7s-map"></i> 
                            <p>Cities</p>
                        </a>
                    </li>
                <?php endif;?>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="provinces"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/provinces">
                            <i class="pe-7s-map"></i> 
                            <p>Provinces</p>
                        </a>
                    </li>
                <?php endif;?>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="zipcodes"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/zipcodes">
                            <i class="pe-7s-map"></i> 
                            <p>Zip Codes</p>
                        </a>
                    </li>
                <?php endif;?>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="settings"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/settings">
                            <i class="pe-7s-map"></i> 
                            <p>About / Contact Us</p>
                        </a>
                    </li>
                <?php endif;?>
                <?php if($this->router->fetch_class()=='admin'):?>
                    <li class="<?php if($this->uri->segment(2)=="sponsors"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>admin/sponsors">
                            <i class="pe-7s-map"></i> 
                            <p>Sponsors</p>
                        </a>
                    </li>
                <?php endif;?>
                <li class="<?php if($this->uri->segment(2)=="user"){echo "active";}?>">
                    <a href="<?php echo base_url().$this->router->fetch_class();?>/user">
                        <i class="pe-7s-user"></i> 
                        <p>User Profile</p>
                    </a>
                </li>
                <!--
                <li>
                    <a href="notifications.html">
                        <i class="pe-7s-bell"></i> 
                        <p>Notifications</p>
                    </a>        
                </li>-->
            </ul> 
    	</div>
    </div>
<?php $this->load->view('admin2/header'); ?>
<?php $this->load->view('admin2/nav'); ?>
<?php $this->load->view('admin2/upper-nav'); ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile</h4>
                            </div>
                            <div class="content">
                                <form id="updateUserInfoForm" action="<?php echo base_url(); echo $this->router->fetch_class();?>/insert_user/<?php echo $user[0]['user_id']; ?>">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" placeholder="First Name" name="fname" value="<?php echo set_value('fname', isset($user[0]['fname']) ? $user[0]['fname'] : '');?>">
                                            </div>        
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" placeholder="Last Name" name="lname" value="<?php echo set_value('lname', isset($user[0]['lname']) ? $user[0]['lname'] : '');?>">
                                            </div>        
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" class="form-control" placeholder="Username" id="username" name="username"
                                value="<?php echo set_value('username', isset($user[0]['username']) ? $user[0]['username'] : '');?>">
                                            </div>        
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" class="form-control" placeholder="Email" id="email" name="email"
                                value="<?php echo set_value('email', isset($user[0]['email']) ? $user[0]['email'] : '');?>">
                                            </div>        
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Cellphone Number</label>
                                                <input type="text" class="form-control" placeholder="Cellphone Number" id="cel-num" name="cel-num" value="<?php echo set_value('cel-num', isset($user[0]['cel-num']) ? $user[0]['cel-num'] : '');?>">
                                            </div>        
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Telephone Number</label>
                                                <input type="text" class="form-control" placeholder="Telephone Number" id="tel-num" name="tel-num" value="<?php echo set_value('tel-num', isset($user[0]['tel-num']) ? $user[0]['tel-num'] : '');?>">
                                            </div>        
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Birthdate</label>
                                                <input type="date" class="form-control" placeholder="Birthdate" id="birth" name="birthdate" value="<?php echo set_value('birthdate', isset($user[0]['birthdate']) ? $user[0]['birthdate'] : '');?>">
                                            </div>        
                                        </div>
                                    </div>
                                    <?php if($this->router->fetch_class()=='admin'):?>
                                        <div class="row">
                                        	<div class="col-md-6">
    			                                <label>Role:</label>
    			                                <select name="role_id" class="form-control">
    			                                    <option value="1" <?php echo (($user[0]['role_id']==1) ? 'selected' : '');?>>Client</option>
    			                                    <option value="2" <?php echo (($user[0]['role_id']==2) ? 'selected' : '');?>>Admin</option>
    			                                    <option value="3" <?php echo (($user[0]['role_id']==3) ? 'selected' : '');?> >Super Admin</option>
    			                                </select>
    			                            </div>
    			                            <div class="col-md-6">
    			                                <label>Status:</label>
    			                                <div style="margin-top: 12px;">
    				                                <label class="radio-inline"><input type="radio" value="1" name="status" <?php echo (($user[0]['status']==1) ? 'checked' : '');?>>Approved</label>
    				                                <label class="radio-inline"><input type="radio" value="0" name="status" <?php echo (($user[0]['status']==0) ? 'checked' : '');?>>Pending</label>
    			                            	</div>
    			                            </div>
                                        </div>
                                    <?php endif;?>
                                    <button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>   
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a href="#">
                                    <img class="avatar border-gray" src="https://app.nimia.com/static/img/default_profile.png" alt="..."/>
                                   
                                      <h4 class="title"><?php echo set_value('fname', isset($user[0]['fname']) ? $user[0]['fname'] : '');?> <?php echo set_value('lname', isset($user[0]['lname']) ? $user[0]['lname'] : '');?><br />
                                         <small><?php echo set_value('username', isset($user[0]['username']) ? $user[0]['username'] : '');?></small>
                                      </h4> 
                                    </a>
                                </div>
                                <!--  
                                <p class="description text-center"> "Lamborghini Mercy <br>
                                                    Your chick she so thirsty <br>
                                                    I'm in that two seat Lambo"
                                </p>-->
                            </div>
                            <hr>
                            <div class="text-center">
                                <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>
    
                            </div>
                        </div>
                    </div>
               
                </div>                    
            </div>
        </div>
<?php $this->load->view('admin2/footer'); ?>

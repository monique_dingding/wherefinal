        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <!--
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               Blog
                            </a>
                        </li>
                    </ul>-->
                </nav>
                <p class="copyright pull-right">
                    &copy; 2015 <a href="http://www.creative-tim.com">Creative Tim</a>
            </div>
        </footer>
        
    </div>   
</div>


</body>

    <!--   Core JS Files   -->
    
	<script src="<?php echo base_url(); ?>assets/admin2/js/bootstrap.min.js" type="text/javascript"></script>
	
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url(); ?>assets/admin2/js/bootstrap-checkbox-radio-switch.js"></script>
	
	<!--  Charts Plugin -->
	<script src="<?php echo base_url(); ?>assets/admin2/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/admin2/js/bootstrap-notify.js"></script>
    
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
	
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="<?php echo base_url(); ?>assets/admin2/js/light-bootstrap-dashboard.js"></script>
	
	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="<?php echo base_url(); ?>assets/admin2/js/demo.js"></script>
	
	<script type="text/javascript">
    	$(document).ready(function(){
        	
        	demo.initChartist();

            var form = $("#userInfoForm");
        $("#userInfoForm").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:$("#userInfoForm").serialize(),//only input
                success: function(response){
                    alert(response);
                    if(response=='Data Inserted Successfully')
                        $('#addUserModal').modal('hide');  
                }
            });
        });

        var form = $("#updateUserInfoForm");
        $("#updateUserInfoForm").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:$("#updateUserInfoForm").serialize(),//only input
                success: function(response){
                    
                    if (response=='Data Updated Successfully' || response=='Data Inserted Successfully'){
                        type = 'success';
                    } else {
                        type = 'warning';
                    }

                    $.notify({
                        icon: 'fa fa-exclamation-triangle',
                        message: response
                    
                    },{
                        type: type,
                        timer: 4000
                    });
                }
            });
        });

        $('#addUserModal').on('hidden.bs.modal', function (e) {
            location.reload();
        });

        $("#repwd").keyup(checkPasswordMatch);

        function checkPasswordMatch() {
            var password = $("#pwd").val();
            var confirmPassword = $("#repwd").val();

            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("<p class='text-danger'>Passwords do not match!</p>");
            else
                $("#divCheckPasswordMatch").html("<p class='text-success'>Passwords match.</p>");
        }
    	});
	</script>
    
</html>
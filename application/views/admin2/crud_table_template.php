<?php $data['is_crud_table'] = $is_crud_table; ?>
<?php $this->load->view('admin2/header'); ?>
<?php $this->load->view('admin2/nav'); ?>
<?php $this->load->view('admin2/upper-nav'); ?>
        <style type='text/css'>
        body
        {
            font-family: Arial;
            font-size: 14px;
        }
        a {
            color: blue;
            text-decoration: none;
            font-size: 14px;
        }
        a:hover
        {
            text-decoration: underline;
        }

        input {
            height: 30px !important;
        }

        .chzn-container-single .chzn-single {
            height: 30px !important;
        }
        h4.title::first-letter {
          text-transform: capitalize;
        }
        </style>        
        <div class="content">
            <div class="container-fluid">    
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?php if($this->uri->segment(2)=="") echo 'Establishments'; else echo $this->uri->segment(2);?></h4>
                                <p class="category"></p>
                            </div>
                            <div class="content">                                
                                <?php echo $output->output; ?>
                                <div class="footer">
                                    <!--
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Open
                                        <i class="fa fa-circle text-danger"></i> Bounce
                                        <i class="fa fa-circle text-warning"></i> Unsubscribe
                                    </div>-->
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> 
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>                   
                </div>        
            </div>    
        </div>
<?php $this->load->view('admin2/footer'); ?>



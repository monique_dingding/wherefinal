<?php $this->load->view('admin/header'); ?>

<style type="text/css">
    td {
        font-size: 12px;
    }
</style>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo ucfirst($title); ?> <small><?php echo ucfirst($small_title); ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <a href="<?php echo base_url(); ?>">Home</a> / 
                                Establishments
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs">
                          <li role="presentation" <?php if ($this->uri->segment(2) == "establishments") echo 'class="active"'; ?>><a href="<?php echo base_url(); ?>admin/establishments">Establishments</a></li>
                          <li role="presentation" <?php if ($this->uri->segment(2) == "featured") echo 'class="active"'; ?>><a href="<?php echo base_url(); ?>admin/featured">Featured</a></li>
                        </ul>
                    </div>
                </div>

                <?php if(isset($message) && $message=="success"): ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success" role="alert">
                            <strong>Success!</strong> The details of <?php echo ucfirst($row['name']) ?> has been successfully updated.
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="row">
                    <br>
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Information
                            </div>
                            <div class="panel-body">
                                <a href="<?php echo base_url(); ?>admin/add_est" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New</a>
                                
                                <table id="esttable" class="table table-condensed table-bordered table-striped">
                                    <thead>
                                        <td><strong>#</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Name</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Featured</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Option</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Service Type</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Owner</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Contact Information</strong>  <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Address</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                        <td><strong>Action</strong></td>
                                    </thead>
                                    <tbody>
                                        <?php $count = 0; ?>
                                        <?php foreach($establishments as $row): ?>
                                        <tr>
                                            <td><?php echo ++$count; ?></td>
                                            <td><?php echo ucfirst($row['name']) ?></td>
                                            <td><?php echo $row['featured'] ?></td>
                                            <td><?php echo ucfirst($row['option']) ?></td>
                                            <td><?php echo $row['service_type'] ?></td>
                                            <td><?php echo ucfirst($row['owner']) ?></td>
                                            <td><?php echo $row['telephone_number'].(empty($row['telephone_number'])?" ":" <br> ").$row['cellphone_number'].(empty($row['telephone_number'])?" ":" <br> ").$row['email']; ?> </td>
                                            <td><?php echo $row['street'].(empty($row['city_name'])?" ":", ").$row['city_name'].(empty($row['province_name'])?" ":", ").$row['province_name'].(empty($row['region_name'])?" ":", ").$row['region_name'].(empty($row['code'])?" ":", Philippines ").$row['code']; ?></td>
                                            <td style="white-space: nowrap;">
                                                <a href="<?php echo base_url(); ?>admin/view_est/<?php echo $row['id'] ?>">View</a> &middot; 
                                                <a href="<?php echo base_url(); ?>admin/update_est/<?php echo $row['id'] ?>">Update</a> &middot; 
                                                <a href="<?php echo base_url(); ?>admin/delete_est/<?php echo $row['id'] ?>" onclick="return confirm('Are you sure you want to delete this establishment?')">Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>

<?php $this->load->view('admin/footer'); ?>

<!-- INSERT JQUERY HERE -->
<script type="text/javascript">
    $(document).ready( function () {
        $('#esttable').DataTable({
            "bJQueryUI": true
        });
    } );
</script>
<?php $this->load->view('admin/header'); ?>

<style type="text/css">
    td {
        font-size: 12px;
    }
</style>


        <div id="page-wrapper">

            <div class="container-fluid">
                <?php foreach($details as $row): ?>

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Update <?php echo ucfirst($row['name']) ?> <small><?php echo ucfirst($small_title); ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <a href="<?php echo base_url(); ?>">Home</a> / 
                                <a href="<?php echo base_url(); ?>admin/establishments">Establishments</a> / 
                                <a href="<?php echo base_url(); ?>">Home</a>
                                <?php echo ucfirst($row['name']) ?>
                            </li>
                            <li>
                                Add New 
                            </li>
                        </ol>
                    </div>
                </div>

                <?php if(isset($message) && $message=="success"): ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success" role="alert">
                            <strong>Success!</strong> The details of <?php echo ucfirst($row['name']) ?> has been successfully updated.
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <!-- Page Heading -->
                <form method="post" action="<?php echo base_url().$link ?>" class="form-horizontal" enctype="multipart/form-data" >
                <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Establishment Information
                                </div>
                                <div class="panel-body">
                                    <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                                    <input type="hidden" name="location_id" value="<?php echo $row['location_id'] ?>">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <td colspan="2">
                                                <?php if (isset($row['cover_image'])): ?>
                                                <img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['cover_image'] ?>" style="height: auto; width: 100%; margin-bottom: 1em;">
                                                <?php else: ?>
                                                <img src="http://placehold.it/655x250" style="margin-bottom: 1em;">
                                                <?php endif ?>

                                                <p class="pull-right">
                                                    <?php if (isset($row['cover_image'])): ?>
                                                    <a href="#" class="btn btn-danger btn-sm pull-right"><i class="fa fa-close"></i> Remove</a>
                                                    <?php else: ?>
                                                    <input type="file" name="coverphoto" class="btn btn-primary btn-sm">
                                                    <?php endif ?>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Name*</strong></td>
                                            <td><input type="text" class="form-control" name="name" value="<?php echo ucfirst($row['name']) ?>"></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Owner*</strong></td>
                                            <td><input type="text" class="form-control" name="owner" value="<?php echo ucfirst($row['owner']) ?>"></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Address*</strong></td>
                                            <td>
                                                <p>Searchable Address Term</p>
                                                <input type="text" name="common_term" class="form-control" placeholder="Enter friendly searchable term" value="<?php echo $row['common_term'] ?>">
                                                <br>

                                                <p>Street Address</p>
                                                <textarea name="street" class="form-control" placeholder="Enter street address"><?php echo $row['street'] ?></textarea>
                                                <br>

                                                <table>
                                                    <tr>
                                                        <td>
                                                            <p>City</p>
                                                            <select name="city_id" class="form-control" style="width: auto; height: 100%;">
                                                                <?php foreach($cities as $c): ?>
                                                                    <option value="<?php echo $c['id']; ?>" <?php if ($row['city_id'] == $c['id']) echo "selected" ?>><?php echo ucfirst($c['city_name']); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </td>
                                                        <td style="padding-left: 1em;">
                                                            <p>Province</p>
                                                            <select name="province_id" class="form-control" style="width: auto; height: 100%;">
                                                                <?php foreach($provinces as $p): ?>
                                                                    <option value="<?php echo $p['id']; ?>" <?php if ($row['province_id'] == $p['id']) echo "selected" ?>><?php echo ucfirst($p['province_name']); ?></option>
                                                                <?php endforeach; ?>
                                                            </select> 
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <p>Region</p>
                                                            <select name="region_id" class="form-control" style="width: auto; height: 100%;">
                                                                <?php foreach($regions as $r): ?>
                                                                    <option value="<?php echo $r['id']; ?>" <?php if ($row['region_id'] == $r['id']) echo "selected" ?>><?php echo ucfirst($r['region_number']); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </td>
                                                        <td style="padding-left: 1em;">
                                                            <p>Zip Code</p>
                                                            <select name="zipcode_id" class="form-control" style="width: auto; height: 100%;">
                                                                <?php foreach($zipcodes as $z): ?>
                                                                    <option value="<?php echo $z['id']; ?>"  <?php if ($row['zipcode_id'] == $z['id']) echo "selected" ?>><?php echo ucfirst($z['code']); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                                
                                        </tr>
                                        <tr>
                                            <td style="white-space: nowrap;"><strong>Contact Details*</strong></td>
                                            <td>
                                                <p>Telephone Number</p>
                                                <input type="text" name="telephone_number" class="form-control" value="<?php echo $row['telephone_number'] ?>">
                                                <br>
                                                <p>Cellphone Number</p>
                                                <input type="text" name="cellphone_number" class="form-control" value="<?php echo $row['cellphone_number'] ?>">
                                                <br>
                                                <p>E-mail Address</p>
                                                <input type="text" name="email" class="form-control" value="<?php echo $row['email'] ?>">
                                                <br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Excerpt*</strong></td>
                                            <td><textarea name="short_summary" class="form-control" placeholder="Excerpt must be 60 words or less"><?php echo ucfirst($row['short_summary']) ?></textarea></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Description*</strong></td>
                                            <td><textarea name="description" class="form-control" placeholder="Description must be 1000 words or less" style="height: 15em"><?php echo ucfirst($row['description']) ?></textarea></td>
                                        </tr>
                                        <tr>
                                            <td style="white-space: nowrap;"><strong>Booking Details*</strong></td>
                                            <td><textarea name="booking_details" class="form-control" placeholder="Description must be 1000 words or less" style="height: 15em"><?php echo ucfirst($row['booking_details']) ?></textarea></td>
                                        </tr>
                                    </table>

                                    <center>
                                    <input type="submit" name="submit" class="btn btn-success" value="Update Details">
                                    <a href="<?php echo base_url() ?>admin/establishments" class="btn btn-default">Cancel</a>
                                    </center>
                                </div>
                            </div>
                        
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Other Details
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td colspan="2">
                                            <?php if (isset($row['cover_image'])): ?>
                                            <img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image'] ?>" style="height: auto; width: 100%; margin-bottom: 1em;">
                                            <?php else: ?>
                                            <img src="http://placehold.it/300x250" style="margin-bottom: 1em;">
                                            <?php endif ?>
                                            <!-- <img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image'] ?>" style="height: auto; width: 100%; margin-bottom: 1em;"> -->
                                            
                                            <p class="pull-right">
                                            <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-o-up"></i> Upload New</a> <?php echo nbs(3); ?>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Remove</a>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Wifi Ready</strong></td>
                                        <td>
                                            <input type="radio" name="wifi_ready" value="yes" <?php if ($row['wifi_ready'] == "yes") echo "checked"; ?>> Yes  <?php echo nbs(4) ?>
                                            <input type="radio" name="wifi_ready" value="no" <?php if ($row['wifi_ready'] == "no") echo "checked"; ?>>  No
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Cards Accepted</strong></td>
                                        <td>
                                            <input type="radio" name="cards_accepted" value="yes" <?php if ($row['cards_accepted'] == "yes") echo "checked"; ?>> Yes  <?php echo nbs(4) ?>
                                            <input type="radio" name="cards_accepted" value="no" <?php if ($row['cards_accepted'] == "no") echo "checked"; ?>>  No
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Price Range</strong></td>
                                        <td><input type="text" class="form-control" name="price" value="<?php echo ucfirst($row['price']) ?>" placeholder="e.g., P2100 to P5600"></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Working Hours*</strong></td>
                                        <td><textarea class="form-control" name="working_hours" placeholder="e.g., Monday to Friday, 9AM-6PM"><?php echo $row['working_hours'] ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Site Page</strong></td>
                                        <?php if(isset($row['friendly_url'])): ?>
                                        <td><a href="<?php echo base_url() ?>f/<?php echo $row['friendly_url'] ?>">http://where.net.ph/f/<?php echo $row['friendly_url'] ?></a></td>
                                        <?php else: ?>
                                        <td> None </td>
                                        <?php endif; ?>
                                    </tr>
                                </table>

                                <center>
                                <input type="submit" name="submit" class="btn btn-success" value="Update Details">
                                <a href="<?php echo base_url() ?>admin/establishments" class="btn btn-default">Cancel</a>
                                </center>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                
                </form>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>

<?php $this->load->view('admin/footer'); ?>

<!-- INSERT JQUERY HERE -->
<script type="text/javascript">
    $(document).ready( function () {
        $('#esttable').DataTable({
            "bJQueryUI": true
        });
    } );
</script>
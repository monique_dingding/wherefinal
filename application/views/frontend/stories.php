
        <div class="page-top" id="templatemo_events">
        </div> <!-- /.page-header -->





		<!-- MIDDLE CONTENT -->
        <div class="middle-content">
            <div class="container" style="margin-top: -80px; margin-bottom: 2%;">
            <?php $this->load->view('frontend/searchbar'); ?>

            </div> <!-- /.container -->
          

            <div class="container">
                
                <div class="row"><!-- second row -->
                    <div class="col-md-10">
                        <h3 class="new-title blue"><strong><i class="glyphicon glyphicon-list-alt"></i> ALL STORIES FROM WHERE </strong></h3>
                        <?php if(!empty($articles[0])):?>
                          <div class="panel panel-default">
                            <div class="panel-body">
                              <div class="col-md-3" style="padding: 0px;">
                                  <img class="article-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $articles[0]['main_image']; ?>" alt="<?php echo $articles[0]['subject']; ?>" >
                              </div>

                              <div class="col-md-9">
                                  <div class="timeline-heading">
                                    <a href="<?php echo base_url().'article/'.$articles[0]["link"]; ?>"><h3 class="new-title"><?php echo $articles[0]['subject']; ?></h3></a>
                                    <p class="desc"><small class="text-muted"> <i class="glyphicon glyphicon-time"></i> <?php echo date('M d, Y h:iA', strtotime($articles[0]['created_at'])); ?><?php echo nbs(5); ?><i class="fa fa-user"></i> <?php echo $articles[0]['author']; ?></small></p>
                                  </div>
                                  <div class="timeline-body">
                                    <p>
                                      <?php $str = rtrim(word_limiter($articles[0]['body'], 50)); 
                                          $str = strip_tags($str);
                                          echo $str;
                                      ?>                                                
                                    </p>
                                    <br>
                                    <?php
                                      $tags = explode(",", $articles[0]['tags']);

                                      foreach($tags as $t) {
                                          $t = trim($t);
                                       ?>
                                          <a href="<?php echo base_url() ?>tags/<?php echo rawurlencode($t); ?>">
                                              <span class="label label-primary"><?php echo $t; ?></span>
                                          </a>
                                    <?php
                                      }
                                    ?>
                                  </div>
                              </div>
                            </div>
                          </div>
                        <?php endif; ?>

                        <?php if(!empty($articles[1])): ?>
                          <div class="panel panel-default">
                            <div class="panel-body">
                              <div class="col-md-3" style="padding: 0px;">
                                  <img class="article-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $articles[1]['main_image']; ?>" alt="<?php echo $articles[1]['subject']; ?>" >
                              </div>

                              <div class="col-md-9">
                                  <div class="timeline-heading">
                                    <a href="<?php echo base_url().'article/'.$articles[0]["link"]; ?>"><h3 class="new-title"><?php echo $articles[1]['subject']; ?></h3></a>
                                    <p class="desc"><small class="text-muted"> <i class="glyphicon glyphicon-time"></i> <?php echo date('M d, Y h:iA', strtotime($articles[1]['created_at'])); ?><?php echo nbs(5); ?><i class="fa fa-user"></i> <?php echo $articles[1]['author']; ?></small></p>
                                  </div>
                                  <div class="timeline-body">
                                    <p>
                                      <?php $str = rtrim(word_limiter($articles[1]['body'], 50)); 
                                          $str = strip_tags($str);
                                          echo $str;
                                      ?>                                                
                                    </p>
                                    <br>
                                    <?php
                                      $tags = explode(",", $articles[1]['tags']);

                                      foreach($tags as $t) {
                                          $t = trim($t);
                                       ?>
                                          <a href="<?php echo base_url() ?>tags/<?php echo rawurlencode($t); ?>">
                                              <span class="label label-primary"><?php echo $t; ?></span>
                                          </a>
                                    <?php
                                      }
                                    ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-2"><!-- third column -->
                        <a href="#">
                            <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/ads4.jpg" style="width: 160px; height: 500px; overflow: hidden;" alt="travel html5 template" title="travel html5 template">
                        </a>
                    </div> <!-- /.col-md-4 -->
                </div>

                <div class="row"><!-- second row -->
                  <div id="ajax_table"></div>
                  <div class="col-md-10">
                    <a class="btn btn-default btn-lg" href="#" role="button" style="color: #314E7D; width: 100%; overflow: hidden;" id="load_more" data-val = "0">Load more articles <i class="glyphicon glyphicon-chevron-right"></i></a>
                  </div>
                </div>
            </div> <!-- /.container -->
        </div> <!-- /.middle-content -->

<script>
  $(document).ready(function(){
      getcountry(0);

      $("#load_more").click(function(e){
          e.preventDefault();
          var page = $(this).data('val');
          getcountry(page);

      });
      //getcountry();
  });

  var getcountry = function(page){
      $("#loader").show();
      $.ajax({
          url:"<?php echo base_url() ?>home/stories_",
          type:'GET',
          data: {page:page, tag: "<?php echo $this->uri->segment(2);?>"}
      }).done(function(response){
          if(response!='empty'){
              $("#ajax_table").append(response);
              $("#loader").hide();
              $('#load_more').data('val', ($('#load_more').data('val')+1));
              //scroll();
          }else{
              $('#load_more').slideUp();
          }
          $.ajax({
              url:"<?php echo base_url() ?>home/stories_",
              type:'GET',
              data: {page:page+1, tag: "<?php echo $this->uri->segment(2);?>"}
          }).done(function(response){
              if(response=='empty'){
                  $('#load_more').slideUp();
              }
              
          });
      });
  };

  var scroll  = function(){
      $('html, body').animate({
          scrollTop: $('#load_more').offset().top
      }, 1000);
  };   
</script>
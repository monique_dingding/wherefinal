<!-- Full Page Image Background Carousel Header -->
<?php echo var_dump($photosliders);?>
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php $count=0;?>
        <?php foreach($photosliders as $row): ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $count;?>" class="<?php echo ($count==0)?'active':'';?>" ></li>
          <?php $count++;?>
        <?php endforeach ?>
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <?php $count=0;?>
        <?php foreach($photosliders as $row): ?>
          <div class="item <?php echo ($count==0)?'active':'';?>">
              <!-- Set the first background image using inline CSS below. -->
              <div class="fill" style="background-image:url('<?php echo base_url(); ?>assets/images/img/<?php echo $row['cover_image'] ?>');"></div>
              <div class="carousel-caption">
                  <h2><?php echo $row['option_name'] ?></h2>
              </div>
          </div>
          <?php $count++;?>
        <?php endforeach ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>

</header>

<div class="site-header" style="top:40%;">
    <div class="container">
        <div class="main-header" style="background:transparent;">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="act-btn">
                        <div class="inner">
                            <div class="price">
                                <i class="fa fa-paper-plane fa-2x"></i>
                            </div> <!-- /.price -->
                            <div class="title">
                                <h2 style="margin-top: 3%; font-size: 25px">Where do you want to go?</h2>
                                <span>

                                   <!--  <input type="text" style="width: 78%"> -->

                                    <select name="option_name" style="width: 78%;">
                                      <?php foreach($options as $row): ?>
                                          <option value="<?php echo $row['option_name'] ?>"><?php echo ucfirst($row['option_name']) ?></option>
                                      <?php endforeach; ?>
                                    </select> 

                                </span>
                            </div>
                        </div> <!-- /.inner -->
                        <a  id="searchsubmit" class="link">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div> <!-- /.act-btn -->
                </div> <!-- /.col-md-8 --> 
            </div> 
        </div> 
    </div> 
</div>

<!--

        <div class="flexslider">
            <ul class="slides">
                <?php foreach($photosliders as $row): ?>
                <li>
                    <div class="overlay"></div>
                    <img class="index-cover-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['cover_image'] ?>" alt="Special 2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-lg-4">
                                <div class="flex-caption visible-lg">
                                    <span class="price bg-green"><i class="glyphicon glyphicon-cutlery"></i> <?php echo $row['option_name'] ?></span>
                                    <h3 class="title"><?php echo $row['name'] ?></h3>
                                    <p><?php echo $row['short_summary'] ?></p>
                                    <a href="<?php echo base_url()."place/".$row['friendly_url'] ?>" class="slider-btn">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <?php endforeach ?>
            </ul>
        </div>  /.flexslider -->

        
        <!-- 
        <div class="container">

            <div class="row">
                <div class="our-listing owl-carousel">

                    <?php foreach($new_locations as $row): ?>
                        <div class="list-item">
                            <div class="list-thumb">
                                <div class="title">
                                    <h4><?php echo $row['province_name'] ?></h4>
                                </div>
                                <img class="location-index-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image'] ?>" alt="<?php echo $row['name'] ?>">
                            </div> 
                            <div class="list-content">
                                <h5><?php echo $row['name'] ?></h5>
                                
                                <a href="<?php echo base_url() ?>place/<?php echo $row['friendly_url']; ?>" class="price-btn">EXPLORE</a>
                            </div> 
                        </div> 
                    <?php endforeach; ?>
                    
                </div> 
            </div> 
        </div> /.container -->

		<div class="middle-content" style="margin-top: 3%;">
           

            <div class="container" style="margin-top: 3%">

                <div class="col-md-8">
                    <h3 class="new-title blue"><strong><i class="glyphicon glyphicon-list-alt"></i> MORE STORIES FROM WHERE </strong></h3>
                    <div id="ajax_table"></div>
                    <a class="btn btn-default btn-lg" href="#" role="button" style="color: #314E7D; width: 100%" id="load_more" data-val = "0">Load more articles <i class="glyphicon glyphicon-chevron-right"></i></a>

                </div>

                <div class="col-md-4">
                    <h3 class="new-title blue"><strong><i class="glyphicon glyphicon-plane"></i> TRAVEL & EXPLORE </strong></h3>

                    <?php $count = 0; ?>
                    <?php foreach($establishments as $row): ?>
                        <?php ++$count; ?>

                        <?php if ($count == 1): ?>
                        <ul class="list-group">
                          <a href="<?php echo base_url() ?>place/<?php echo $row['friendly_url']; ?>">
                              <li class="list-group-item img-list" ><img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image']; ?>" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template"></li>
                              <li class="list-group-item">
                                <h3 class="new-title"><?php echo $row['name'] ?>
                                    <br>
                                    <small><i class="glyphicon glyphicon-map-marker"></i> <?php echo $row['city_name'] ?>, <?php echo $row['region_name'] ?></small>
                                    <br>
                                    <small><i class="fa fa-money"></i> <?php echo $row['price'] ?></small>
                                     </h3> 
                              </li>
                          </a>

                          <br>

                          <!-- ADS HERE! -->
                          <a href="#">
                            <img src="<?php echo base_url(); ?>assets/images/ads2.jpg" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template">
                          </a>
                        </ul>
                        <?php endif; ?>

                        <?php if ($count == 2): ?>
                        <ul class="list-group">
                          <a href="#">
                              <li class="list-group-item img-list" ><img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image']; ?>" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template"></li>
                              <li class="list-group-item">
                                <h3 class="new-title blue"><?php echo $row['name'] ?>
                                    <br>
                                    <small><i class="glyphicon glyphicon-map-marker"></i> <?php echo nbs(3); ?><?php echo $row['street'].", ".$row['city_name'].", <br>".nbs(5).$row['province_name'].", Philippines"; ?> </small>
                                    <br>
                                    <small><i class="fa fa-money"></i> <?php echo $row['price'] ?></small>
                                     </h3> 
                              </li>
                          </a>
                          <br>    
                        <?php endif; ?>

                        <?php if ($count == 3): ?>
                        <ul class="list-group">
                          <a href="#">
                              <li class="list-group-item img-list" ><img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image']; ?>" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template"></li>
                              <li class="list-group-item">
                                <h3 class="new-title black"><?php echo $row['name'] ?>
                                    <br>
                                    <small><i class="glyphicon glyphicon-map-marker"></i> Pamalican Island, Palawan</small>
                                    <br>
                                    <small><i class="fa fa-money"></i> <?php echo $row['price'] ?></small>
                                     </h3> 
                              </li>
                          </a>

                          <br>

                          <!-- ADS HERE! -->
                          <a href="#">
                            <img src="<?php echo base_url(); ?>assets/images/ads1.jpg" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template">
                          </a>
                        </ul>
                        <?php endif; ?>

                    <?php endforeach; ?>

<!--                     


                    <ul class="list-group">
                      <a href="#">
                          <li class="list-group-item img-list" ><img src="<?php echo base_url(); ?>assets/images/tinagofalls1.jpg" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template"></li>
                          <li class="list-group-item">
                            <h3 class="new-title black">Tinago Falls <small><i class="glyphicon glyphicon-map-marker"></i> Lanao del Norte, Philippines</small> </h3> 
                          </li>
                      </a>
                    </ul>

                    <ul class="list-group">
                      <a href="#">
                          <li class="list-group-item img-list" ><img src="<?php echo base_url(); ?>assets/images/amanpulo1.jpg" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template"></li>
                          <li class="list-group-item">
                            <h3 class="new-title black">Amanpulo in Pamalican Island <small><i class="glyphicon glyphicon-map-marker"></i> Pamalican Island, Palawan</small> </h3> 
                          </li>
                      </a>
                    </ul>


                    <a href="#">
                        <img src="<?php echo base_url(); ?>assets/images/ads1.jpg" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template">
                    </a> -->

                    
                </div>

            </div>

        </div>
<script>
$(document).ready(function(){
                getcountry(0);

                $("#load_more").click(function(e){
                    e.preventDefault();
                    var page = $(this).data('val');
                    getcountry(page);

                });
                //getcountry();
            });

            var getcountry = function(page){
                $("#loader").show();
                $.ajax({
                    url:"<?php echo base_url() ?>home/articles",
                    type:'GET',
                    data: {page:page}
                }).done(function(response){
                    if(response!='empty'){
                        $("#ajax_table").append(response);
                        $("#loader").hide();
                        $('#load_more').data('val', ($('#load_more').data('val')+1));
                       // scroll();
                    }else{
                        $('#load_more').slideUp();
                    }
                    $.ajax({
                        url:"<?php echo base_url() ?>home/articles",
                        type:'GET',
                        data: {page:page+1}
                    }).done(function(response){
                        if(response=='empty'){
                            $('#load_more').slideUp();
                        }
                    });
                });
            };

            var scroll  = function(){
                $('html, body').animate({
                    scrollTop: $('#load_more').offset().top
                }, 1000);
            };
        </script>
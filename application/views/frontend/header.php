<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Where? - For Travellers </title>
        <meta name="description" content="">
<!-- 
Travel Template
http://www.templatemo.com/tm-409-travel
-->
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="templatemo">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/templatemo_misc.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/templatemo_style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timeline.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/full-slider.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.11.0.min.js"></script>

    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url(); ?>assets/images/where-header-logo.png" style="width: 100px; height: auto;" alt="travel html5 template" title="travel html5 template">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav pull-right">
                        <li <?php if($page=="index") echo 'class="active"'; ?>><a href="<?php echo base_url() ?>">Home</a></li>
                        <li <?php if($page=="featured") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>featured">Featured</a></li>
                        <li <?php if($page=="destinations") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>destinations">Destinations</a></li>
                        <li <?php if($page=="stories") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>stories">Stories</a></li>
                        <li <?php if($page=="create-page") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>create-page">Create Your Page</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>


        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
<!--
        <div class="site-header">
            <div class="container">
                <div class="main-header">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-10">
                            <div class="logo" style="margin-top: 0.75em;">
                                <a href="<?php echo base_url(); ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/where-header-logo.png" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template">
                                </a>
                            </div> 
                        </div> 
                        <div class="col-md-8 col-sm-6 col-xs-2">
                            <div class="main-menu">
                                <ul class="visible-lg visible-md">
                                    <li <?php if($page=="index") echo 'class="active"'; ?>><a href="<?php echo base_url() ?>">Home</a></li>
                                    <li <?php if($page=="featured") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>featured">Featured</a></li>
                                    <li <?php if($page=="destinations") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>destinations">Destinations</a></li>
                                    <li <?php if($page=="stories") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>stories">Stories</a></li>
                                    <li <?php if($page=="create-page") echo 'class="active sub-header"' ?>><a href="<?php echo base_url() ?>create-page">Create Your Page</a></li>
                                </ul>
                                <a href="#" class="toggle-menu visible-sm visible-xs">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-12 visible-sm visible-xs">
                        <div class="menu-responsive">
                            <ul>
                                <li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="services.html">Services</a></li>
                                <li><a href="events.html">Events</a></li>
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div>  /.site-header -->



        <div class="page-top" id="templatemo_events">
        </div> <!-- /.page-header -->





		<!-- MIDDLE CONTENT -->
        <div class="middle-content">
            <div class="container" style="margin-top: -80px; margin-bottom: 2%;">
            <?php $this->load->view('frontend/searchbar'); ?>
            </div> <!-- /.container -->

            <div class="container">
                
                <div class="row"><!-- second row -->

                    <?php foreach($featured_establishments as $row): ?>
                    <div class="col-md-4"><!-- first column -->
                        <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="widget-item">
                                <!-- <h3 class="widget-title">Food</h3> -->
                                <a href="<?php echo base_url() ?>place/<?php echo $row['friendly_url']; ?>">
                                    <div class="sample-thumb">
                                        <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image']; ?>" alt="New Event" title="New Event">
                                    </div> <!-- /.sample-thumb -->
                                    <h4 class="consult-title"><?php echo ucfirst($row['name']) ?>
                                        <br>
                                        <small><i class="glyphicon glyphicon-map-marker"></i> <?php echo nbs(3); ?><?php echo $row['street'].", ".$row['city_name'].", <br>".nbs(5).$row['province_name'].", Philippines"; ?> </small>
                                    </h4>
                                </a>
                                <p>
                                    <?php $str = rtrim(word_limiter($row['description'], 40)); 
                                        $str = strip_tags($str);
                                        echo $str;
                                    ?>   
                                    <br>
                                    <a href="<?php echo base_url() ?>place/<?php echo $row['friendly_url']; ?>">Read more</a>
                                </p>
                            </div> <!-- /.widget-item -->
                          </div> <!-- /.col-md-4 -->
                        </div> <!-- /.col-md-4 -->
                    </div> <!-- /.col-md-4 -->
                    <?php endforeach; ?>

                </div> <!-- /.row second -->
                
                <div class="row" id="ajax_table"><!-- second row -->     
                    <div class="col-md-3"><!-- third column -->
                        <a href="#">
                            <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/ads3.jpg" style="width: 100%; height: auto;" alt="travel html5 template" title="travel html5 template">
                        </a>
                    </div> <!-- /.col-md-4 -->
                </div> <!-- /.row second -->
                    <br>
            <a class="btn btn-default btn-lg" href="#" role="button" style="color: #314E7D; width: 100%; overflow: hidden;" id="load_more" data-val = "0">Load more articles <i class="glyphicon glyphicon-chevron-right"></i></a>

            </div> <!-- /.container -->
        </div> <!-- /.middle-content -->

 <script>
  $(document).ready(function(){
      getcountry(0);

      $("#load_more").click(function(e){
          e.preventDefault();
          var page = $(this).data('val');
          getcountry(page);

      });
      //getcountry();
  });

  var getcountry = function(page){
      $("#loader").show();
      $.ajax({
          url:"<?php echo base_url() ?>home/featured_articles",
          type:'GET',
          data: {page:page}
      }).done(function(response){
          if(response!='empty'){
              $("#ajax_table").append(response);
              $("#loader").hide();
              $('#load_more').data('val', ($('#load_more').data('val')+1));
              //scroll();
          }else{
              $('#load_more').slideUp();
          }
          $.ajax({
              url:"<?php echo base_url() ?>home/featured_articles",
              type:'GET',
              data: {page:page+1}
          }).done(function(response){
              if(response=='empty'){
                  $('#load_more').slideUp();
              }
          });
      });
  };

  var scroll  = function(){
      $('html, body').animate({
          scrollTop: $('#load_more').offset().top
      }, 1000);
  };   
</script>
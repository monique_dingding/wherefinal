
        <div class="page-top" id="templatemo_events">
        </div> <!-- /.page-header -->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker-standalone.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" />
    
    <!-- include your less or built css files  -->
    <!-- 
    bootstrap-datetimepicker-build.less will pull in "../bootstrap/variables.less" and "bootstrap-datetimepicker.less";
    or
    -->


		<!-- MIDDLE CONTENT -->
        <div class="middle-content">
            <div class="container" style="margin-top: -80px; margin-bottom: 2%;">
            <?php $data['options'] = $options; ?>
            <?php $this->load->view('frontend/searchbar', $data); ?>

            </div> <!-- /.container -->
            
            <div class="container">
              <div class="col-md-8">
                <h3 class="widget-title montserrat" style="margin-bottom: 0px;">SIGN UP TO CREATE A PAGE</h3>
                <h1 style="margin-bottom: 3%;" class="montserrat">Something introduction here</h1>
                <div class="contact-form msgSuccess">
                  <div class="alert alert-success montserrat" role="alert" style="font-size: 120%">

                    <strong>Success!</strong> <br>
                    Your profile will be evaluated for approval by the administrator. Please wait for an e-mail response within the next few days.
                    <br><br><a href="<?php echo base_url() ?>">GO BACK TO HOME</a>
                  </div>  
                </div>

                <div class="contact-form add-user">
                    <form name="contactform" id="contactform" method="post">
                        <p>
                            <input name="fname" type="text" id="fname" placeholder="First Name" required>
                        </p><p>
                            <input name="lname" type="text" id="lname" placeholder="Last Name" required>
                        </p>
                        <p>
                            <input name="email" type="text" id="email" placeholder="Your Email" required> 
                        </p>
                        <p>
                            <input type='text' id='datetimepicker4' placeholder="Birthdate" name="birthday" required>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker4').datetimepicker({
                                      defaultDate: false,
                                      format: 'YYYY-MM-DD',
                                    });

                                });
                            </script>
                        </p>
                        <p>
                            <input name="cel_num" type="text" id="mobile" placeholder="Mobile Number" required autocomplete="false"> 
                        </p>
                         <p>
                            <input name="pwd" type="password" id="password" placeholder="Password" autocomplete="new-password" value=""> 
                        </p>
                        <input type="submit" class="mainBtn" id="submit" value="Submit details">
                    </form>
                </div> <!-- /.contact-form -->
              </div>
            </div> <!-- /.container -->

        </div> <!-- /.middle-content -->

        <script type="text/javascript">
          $( document ).ready(function() {
            $(".msgSuccess").hide();

            $("#contactform").on("submit", function(e){
              e.preventDefault();

              $.ajax({
                method: "POST",
                url: "<?php echo base_url() ?>submit_client",
                data: { 
                  fname: $("#fname").val(),
                  lname: $("#lname").val(),
                  email: $("#email").val(),
                  cel_num: $("#mobile").val(),
                  password: $("#password").val(),
                }
              })
              .done(function(result) {
                $(".add-user").hide();
                $(".msgSuccess").show();
              });

            });


          });
        

        </script>

 
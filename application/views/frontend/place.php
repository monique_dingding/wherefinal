
        <div class="page-top" id="templatemo_events"  style="margin-bottom: -5% !important;">
        </div> <!-- /.page-header -->

		<!-- MIDDLE CONTENT -->
        <div class="middle-content" >
            <div class="container">
            
                <div class="row"><!-- first row -->
                    <?php $this->load->view('frontend/searchbar'); ?>
                    <br>
                </div>

                <div class="row"><!-- first row -->
                
                    <div class="col-md-12"><!-- second column -->
                        <center>

                            <div class="panel panel-default" style="background-color: #00AE98 !important;">
                                <div class="panel-body">
                                <h3 class="lg-font-size montserrat white"><?php echo strtoupper($query[0]['name']) ?></h3>
                                <span class="montserrat white" style="font-size: 120%;"><?php echo $query[0]['short_summary'] ?></span>
                                </div>
                            </div>
                        </center>

                        <div class="panel panel-body">
                            <div class="sample-thumb">
                                <img class="article-primary-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['cover_image'] ?>" alt="<?php echo $query[0]['name'] ?>" title="<?php echo $query[0]['name'] ?>">
                            </div> <!-- /.sample-thumb -->
                        </div>

                    </div> <!-- /.col-md-4 -->
                    

                    <div class="col-md-4"><!-- second column -->
                        <div class="panel panel-body">
                            <div class="sample-thumb">
                            <img class="article-logo-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['logo'] ?>" alt="<?php echo $query[0]['name'] ?>" title="<?php echo $query[0]['name'] ?>">
                            </div>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <center>
                                    <span class="montserrat" style="color: #00AE98; font-size: 160%"><strong>CONTACT US</strong></span>
                                </center>
                            </li>
                            
                            <li class="list-group-item">
                                <?php if(!empty($query[0]['address'])): ?>
                                    <span class="montserrat"><strong>ADDRESS</strong></span> <br>
                                    <?php echo $query[0]['address'] ?> <br>
                                    <a href="http://maps.google.com/?q=<?php echo $query[0]['address'] ?>" target="_blank"> <i class="fa fa-cab"></i> Give me directions</a>
                                    <br><br>
                                <?php endif; ?>

                                <?php if(!empty($query[0]['website'])): ?>
                                    <span class="montserrat"><strong>WEBSITE</strong></span> <br>
                                    <a href="<?php echo $query[0]['website'] ?>">
                                    <?php echo $query[0]['website'] ?>
                                    </a>
                                    <br><br>
                                <?php endif; ?>

                                <?php if(!empty($query[0]['email'])): ?>
                                    <span class="montserrat"><strong>E-MAIL ADDRESS</strong></span> <br>
                                    <a href="mailto:<?php echo $query[0]['website'] ?>?Subject:Contact%20Us">
                                    <?php echo $query[0]['email'] ?>
                                    </a>
                                    <br><br>
                                <?php endif; ?>
                                
                                <?php if(!empty($query[0]['cellphone_number'])): ?>
                                    <span class="montserrat"><strong>CELLPHONE NUMBER</strong></span> <br>
                                    <a href="tel:<?php echo $query[0]['cellphone_number'] ?>">
                                        <?php echo $query[0]['cellphone_number'] ?>
                                    </a>
                                    <br><br>
                                <?php endif; ?>

                                <?php if(!empty($query[0]['telephone_number'])): ?>
                                    <span class="montserrat"><strong>TELEPHONE NUMBER</strong></span> <br>
                                    <a href="tel:<?php echo $query[0]['cellphone_number'] ?>">
                                    <?php echo $query[0]['telephone_number'] ?>
                                    </a>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-8"><!-- second column -->

                        <span style="line-height: 1.75 !important; color: #777 !important;">
                            <div class="col-md-12">
                                <span class="montserrat" style="color: #00AE98; font-size: 160%"><strong>DESCRIPTION</strong></span> <br>
                                <?php echo $query[0]['description']; ?>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <span class="montserrat" style="color: #00AE98; font-size: 160%"><strong>BOOKING DETAILS</strong></span></li>
                                <?php echo $query[0]['booking_details'] ?>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <span class="montserrat" style="color: #00AE98; font-size: 160%"><strong>MORE INFORMATION</strong></span></li>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                    <?php 
                                        if($query[0]['cards_accepted'] == "yes") { echo '<i class="fa fa-check blue"></i>'; }
                                        else { echo '<i class="fa fa-times fa-1x reddy" style="color: #EE5662"></i>'; } ?>
                                    CARDS ACCEPTED
                                    </li>

                                    <li class="list-group-item">
                                    <?php 
                                        if($query[0]['wifi_ready'] == "yes") { echo '<i class="fa fa-check blue"></i>'; }
                                        else { echo '<i class="fa fa-times fa-1x reddy" style="color: #EE5662"></i>'; } ?>
                                        WIFI READY
                                    </li>

                                    <li class="list-group-item"><i class="fa fa-money blue"></i> <?php echo $query[0]['price'] ?></li>
                                    <li class="list-group-item"><i class="fa fa-building blue"></i> <?php echo $query[0]['owner'] ?></li>
                                </ul>
                            </div>
                            <br>
                        </span>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-3">
                            <a href="#" class="thumbnail">
                            <img class="article-secondary-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['sub_image1'] ?>" alt="<?php echo $query[0]['name'] ?>" title="<?php echo $query[0]['name'] ?>">
                            </a>
                        </div>

                        <div class="col-md-3">
                            <a href="#" class="thumbnail">
                            <img class="article-secondary-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['sub_image2'] ?>" alt="<?php echo $query[0]['name'] ?>" title="<?php echo $query[0]['name'] ?>">
                            </a>
                        </div>

                        <div class="col-md-3">
                            <a href="#" class="thumbnail">
                            <img class="article-secondary-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['sub_image3'] ?>" alt="<?php echo $query[0]['name'] ?>" title="<?php echo $query[0]['name'] ?>">
                            </a>
                        </div>

                        <div class="col-md-3">
                            <a href="#" class="thumbnail">
                            <img class="article-secondary-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['sub_image4'] ?>" alt="<?php echo $query[0]['name'] ?>" title="<?php echo $query[0]['name'] ?>">
                            </a>
                        </div>
                    </div>
                    

               
                </div> <!-- /.row first -->

        </div> <!-- /.middle-content -->

 
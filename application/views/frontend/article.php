
        <div class="page-top" id="templatemo_events"  style="margin-bottom: -5% !important;">
        </div> <!-- /.page-header -->

		<!-- MIDDLE CONTENT -->
        <div class="middle-content" >
            <div class="container">
            
                <div class="row"><!-- first row -->
                    <?php $this->load->view('frontend/searchbar'); ?>
                    <br>
                </div>

                <div class="row"><!-- first row -->
                
                    <div class="col-md-10"><!-- second column -->
                        <div class="widget-item">
                            <h3 class="widget-title opensans"><?php echo $query[0]['subject']; ?></h3>
                            <h4 class="consult-title" style="margin-top: 0px; color: #314E7D !important;"><i class="glyphicon glyphicon-time"></i> <?php echo date("M d, Y h:iA", strtotime($query[0]['created_at'])); ?> <?php echo nbs(6); ?> <i class="fa fa-user"></i> <?php echo $query[0]['author']; ?></h4>
                            <div class="sample-thumb">
                                <img class="article-primary-image" src="<?php echo base_url(); ?>assets/images/img/<?php echo $query[0]['main_image'] ?>" alt="<?php echo $query[0]['subject'] ?>" title="<?php echo $query[0]['subject'] ?>">
                            </div> <!-- /.sample-thumb -->
                            <p style="font-family: 'Open Sans' !important; ">
                                <br>
                                <?php echo $query[0]['body']; ?>
                            </p>

                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-4 -->

                    <div class="col-md-2"><!-- third column -->
                        <br>
                        <br>
                        <br>
                        <a href="#">
                            <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/ads3.jpg" style="width: 160px; height: 400px; overflow: hidden;" alt="travel html5 template" title="travel html5 template">
                        </a>
                    </div> <!-- /.col-md-4 -->
                    
                </div> <!-- /.row first -->

                <div class="our-listing owl-carousel">
                    <?php foreach($articles as $row): ?>

                    <div class="list-item">
                        <div class="list-thumb">
                            <!-- <div class="title">
                                <h4>PANGLAO</h4>
                            </div> -->
                            <img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['main_image']; ?>" style="width: 255px; height: 160px; overflow: hidden;">
                        </div> <!-- /.list-thumb -->
                        <div class="list-content">
                            <h5 style="font-size: 15px !important;"><?php echo word_limiter($row['subject'], 10); ?></h5>
                            <span><?php echo $row['author'] ?></span>
                            <a href="<?php echo base_url() ?>article/<?php echo $row['link'] ?>" class="price-btn">READ MORE</a>
                        </div> <!-- /.list-content -->
                    </div> <!-- /.list-item -->
                    <?php endforeach; ?>
                </div> <!-- /.our-listing -->
          
            </div> <!-- /.container -->
        </div> <!-- /.middle-content -->

 
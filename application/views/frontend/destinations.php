
        <div class="page-top" id="templatemo_events">
        </div> <!-- /.page-header -->





		<!-- MIDDLE CONTENT -->
        <style type="text/css">
        .widget-title {
            font-size: 24px !important;
            margin-bottom: 5% !important;
        }
        </style>
        
        <div class="middle-content">
            <div class="container" style="margin-top: -80px; margin-bottom: 2%;">
            <?php $this->load->view('frontend/searchbar'); ?>

            </div> <!-- /.container -->
            
            <div class="container">
                
                <div class="row"><!-- second row -->

                </div>
                <div class="row"><!-- second row -->
                    <?php for($i=0; $i<=1; $i++): ?>
                    <?php if(!empty($establishments[$i])):?>
                    <div class="col-md-5"><!-- first column -->
                        <div class="widget-item">
                            <a href="<?php echo base_url() ?>place/<?php echo $establishments[$i]['friendly_url'] ?>">
                                <h3 class="widget-title"><?php echo $establishments[$i]['name'] ?> <br><small><i class="fa fa-map-marker"></i> <?php echo nbs(3); ?><?php echo $establishments[$i]['street'].", ".$establishments[$i]['city_name'].", <br>".nbs(5).$establishments[$i]['province_name'].", Philippines"; ?> </small></h3>
                                <div class="sample-thumb">
                                    <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $establishments[$i]['image'] ?>" alt="<?php echo $establishments[$i]['name'] ?>" title="<?php echo $establishments[$i]['name'] ?>">
                                </div> <!-- /.sample-thumb -->
                            </a>
                            <h4 class="consult-title"><?php echo $establishments[$i]['short_summary'] ?></h4>
                            <p> 
                                <?php $str = rtrim(word_limiter($establishments[$i]['description'], 40)); 
                                    $str = strip_tags($str);
                                    echo $str;
                                ?>  <br>
                                <a href="#" class="bluey">Read more</a></p>
                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-4 -->
                  <?php endif;?>
                    <?php endfor; ?>

                    <!-- ADS HERE -->
                    <div class="col-md-2"><!-- third column -->
                        <a href="#">
                            <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/ads4.jpg" style="width: 160px; height: 400px; overflow: hidden;" alt="travel html5 template" title="travel html5 template">
                        </a>
                    </div> <!-- /.col-md-4 -->
                </div> <!-- /.row second -->

                <div class="row"><!-- second row -->
                    <div class="col-md-2"><!-- third column -->
                        <a href="#">
                            <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/ads7.jpg" style="width: 160px; height: 400px; overflow: hidden;" alt="travel html5 template" title="travel html5 template">
                        </a>
                    </div> <!-- /.col-md-4 -->
                    <?php for($i=2; $i<=3; $i++): ?>
                    <?php if(!empty($establishments[$i])):?>
                    <div class="col-md-5"><!-- first column -->
                            <div class="widget-item">
                                <a href="<?php echo base_url() ?>place/<?php echo $establishments[$i]['friendly_url'] ?>">
                                    <h3 class="widget-title"><?php echo $establishments[$i]['name'] ?> <br><small><i class="fa fa-map-marker"></i> <?php echo nbs(3); ?><?php echo $establishments[$i]['street'].", ".$establishments[$i]['city_name'].", <br>".nbs(5).$establishments[$i]['province_name'].", Philippines"; ?> </small></h3>
                                    <div class="sample-thumb">
                                        <img class="featured-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $establishments[$i]['image'] ?>" alt="<?php echo $establishments[$i]['name'] ?>" title="<?php echo $establishments[$i]['name'] ?>">
                                    </div> <!-- /.sample-thumb -->
                                </a>
                                <h4 class="consult-title"><?php echo $establishments[$i]['short_summary'] ?></h4>
                                <p> 
                                    <?php $str = rtrim(word_limiter($establishments[$i]['description'], 40)); 
                                        $str = strip_tags($str);
                                        echo $str;
                                    ?>  <br>
                                     <a href="#" class="bluey">Read more</a></p>
                            </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-4 -->
                  <?php endif;?>
                    <?php endfor; ?> 
                    

                </div> <!-- /.row second -->
                <div class="row" id="ajax_table"></div>
                <div class="row"><!-- second row -->                
                    <a class="btn btn-default btn-lg" href="#" role="button" style="color: #314E7D; width: 100%; overflow: hidden;" id="load_more" data-val = "1">Load more articles <i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>

            </div> <!-- /.container -->
        </div> <!-- /.middle-content -->

 <script>
  $(document).ready(function(){
      getcountry(1);

      $("#load_more").click(function(e){
          e.preventDefault();
          var page = $(this).data('val');
          getcountry(page);

      });
      //getcountry();
  });

  var getcountry = function(page){
      $("#loader").show();
      $.ajax({
          url:"<?php echo base_url() ?>home/establishments",
          type:'GET',
          data: {page:page}
      }).done(function(response){
          if(response!='empty'){
              $("#ajax_table").append(response);
              $("#loader").hide();
              $('#load_more').data('val', ($('#load_more').data('val')+1));
              //scroll();
          }else{
              $('#load_more').slideUp();
          }
          $.ajax({
              url:"<?php echo base_url() ?>home/establishments",
              type:'GET',
              data: {page:page+1}
          }).done(function(response){
              if(response=='empty'){
                  $('#load_more').slideUp();
              }
          });
      });
  };

  var scroll  = function(){
      $('html, body').animate({
          scrollTop: $('#load_more').offset().top
      }, 1000);
  };   
</script>
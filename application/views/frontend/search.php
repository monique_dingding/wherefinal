
        <div class="page-top" id="templatemo_events">
        </div> <!-- /.page-header -->


		<!-- MIDDLE CONTENT -->
        <div class="middle-content">
            <div class="container" style="margin-top: -80px; margin-bottom: 2%;">
            <?php $data['options'] = $options; ?>
            <?php $this->load->view('frontend/searchbar', $data); ?>

            </div> <!-- /.container -->
            
            <div class="container">
              <div class="col-md-8">
                <h3 class="new-title blue"><strong> <i class='fa fa-search'></i> <?php echo strtoupper($option_details[0]['option_statement']); ?> </strong> <small> <?php echo (count($list)>1)?count($list)." results":count($list)." result"; ?></small></h3>
                <?php if(!empty($option_details[0]['option_image'])): ?>
                <img class="search-image" src="<?php echo base_url(); ?>assets/images/<?php echo $option_details[0]['option_image']; ?>" >
                <?php endif; ?>
                <br>

                <?php if (!empty($list)): ?>
                  <?php foreach ($list as $row): ?>
                    <div class="panel panel-default">
                      <div class="panel-body">  
                        <div class="col-md-3" style="padding: 0px;">
                            <img class="article-crop" src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image']; ?>" alt="<?php echo $row['name']; ?>" >
                        </div>

                        <div class="col-md-9">
                          <div class="timeline-heading" style="padding-left: 15%;">
                            <a href="<?php echo base_url(); ?>place/<?php echo $row['friendly_url'] ?>"><h3 class="new-title"><?php echo $row['name']; ?></h3></a>
                            <p class="sm-font-size">
                              <i class="fa fa-map-marker"></i><?php echo nbs(3); ?><?php echo $row['street'].", ".$row['city_name'].", <br>".nbs(5).$row['province_name'].", Philippines"; ?> 
                              <br>
                              <i class="fa fa-cab"></i> <a href="http://maps.google.com.ph/?q=<?php echo $row['name']." ".$row['street'].", ".$row['city_name']; ?>" target="_blank"> Give me directions</a>
                              <br>
                              <?php if (isset($row['telephone_number'])): ?><i class="fa fa-phone"></i> <?php echo $row['telephone_number'] ?> <?php endif; ?>
                              <?php echo nbs(3); ?>
                              <?php if (isset($row['cellphone_number'])): ?><i class="fa fa-mobile"></i> <?php echo $row['cellphone_number'] ?> <?php endif; ?>
                              <br>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                <?php endforeach; ?>
                <?php else: ?>
                 <p>No results found.</p>
                <?php endif; ?>

                <?php if(count($list)>10): ?>
                    <a class="btn btn-default btn-lg" href="#" role="button" style="color: #314E7D; width: 100%">Load more articles <i class="glyphicon glyphicon-chevron-right"></i></a>
                <?php endif; ?>

              </div> <!-- /.container -->
            </div> <!-- /.container -->

        </div> <!-- /.middle-content -->

 
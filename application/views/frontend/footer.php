	       <!-- PARTNER LIST -->
        <div class="partner-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="partner-item">
                            <img src="<?php echo base_url(); ?>assets/images/partners/cebpac.png" style="width: 100%; height: auto;" alt="">
                        </div> <!-- /.partner-item -->
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="partner-item">
                            <img src="<?php echo base_url(); ?>assets/images/partners/airasia.png" style="width: 80%; height: auto;" alt="">
                        </div> <!-- /.partner-item -->
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="partner-item">
                            <img src="<?php echo base_url(); ?>assets/images/partners/pal.png" style="width: 90%; height: auto;" alt="">
                        </div> <!-- /.partner-item -->
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="partner-item">
                            <img src="<?php echo base_url(); ?>assets/images/partners/insular.png" style="width: 100%; height: auto;" alt="">
                        </div> <!-- /.partner-item -->
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="partner-item">
                            <img src="<?php echo base_url(); ?>assets/images/partners/dot.png" style="width: 40%; height: auto;" alt="">
                        </div> <!-- /.partner-item -->
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="partner-item last">
                            <img src="<?php echo base_url(); ?>assets/images/partners/davao.png" style="width: 40%; height: auto;" alt="">
                        </div> <!-- /.partner-item -->
                    </div> <!-- /.col-md-2 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.partner-list -->



        <div class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="footer-logo">
                            <a href="index.html">
                                <img src="<?php echo base_url(); ?>assets/images/where-header-logo.png" style="width: 100%; height: auto;" alt="">
                            </a>
                        </div>
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="copyright">
                            <span>
                            	<!-- 
                                Copyright &copy; 2016 <a href="#">Monique Dingding & Eldrin Librea</a>
                                <br> -->
                                
                                <a href="<?php echo base_url() ?>about">About</a> <br>                    
                                <a href="<?php echo base_url() ?>contact">Contact Us</a> <br>                    
                                <a href="<?php echo base_url() ?>destinations">Establishments</a> <br>                    
                                <a href="<?php echo base_url() ?>create-page">Create a Page</a> <br>                    
                                    
                            <!--
                            | Design: <a rel="nofollow" href="http://www.templatemo.com" target="_parent">templatemo</a>
                            -->
                            
                            </span>
                        </div>
                    </div> <!-- /.col-md-4 -->
                     <div class="col-md-4 col-sm-4">
                        <!-- <ul class="social-icons">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                            <li><a href="#" class="fa fa-flickr"></a></li>
                            <li><a href="#" class="fa fa-rss"></a></li>
                        </ul> -->
                        <ul class="social-icons">
                        Check us out on:
                            <li><a href="https://www.facebook.com/wherenetph/?fref=ts" target="_blank" class="fa fa-facebook"></a></li>
                        </ul>
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.site-footer -->

        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <!-- templatemo 409 travel -->  


        <!-- Google Map -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.gmap3.min.js"></script>
        
        <!-- Google Map Init-->
        <script type="text/javascript">
            var tmp;

                
            $.getJSON('http://ipinfo.io', function(data){
                tmp = data['loc'].split(",");
                $('.first-map').gmap3({
                    marker:{
                        address: tmp[0]+","+tmp[1] 
                    },
                    map:{
                        options:{
                            zoom: 16,
                            scrollwheel: false,
                            streetViewControl : true
                        }
                    }
                });
            });

            </script>
        <!-- templatemo 409 travel -->
    </body>
</html>
<form method="post" class="searchForm" action="<?php echo base_url(); ?>subsearch">
<div class="row">
    <div class="col-md-4">
        <div class="first-map"></div>
    </div> <!-- /.col-md-4 -->
    <div class="col-md-8">
        <div class="act-btn">
            <div class="inner">
                <div class="price">
                    <i class="fa fa-paper-plane fa-2x"></i>
                </div> <!-- /.price -->
                <div class="title">
                    <h2 style="margin-top: 3%; font-size: 25px">Where do you want to go?</h2>
                    <span>

                       <!--  <input type="text" style="width: 78%"> -->

                        <select name="option_name" style="width: 78%;">
                          <?php foreach($options as $row): ?>
                              <option value="<?php echo $row['option_name'] ?>"><?php echo ucfirst($row['option_name']) ?></option>
                          <?php endforeach; ?>
                        </select> 

                    </span>
                </div>
            </div> <!-- /.inner -->
            <a  id="searchsubmit" class="link">
                <i class="fa fa-angle-right"></i>
            </a>
        </div> <!-- /.act-btn -->
    </div> <!-- /.col-md-8 -->
</div> <!-- /.row -->
</form>

<script type="text/javascript">

$("#searchsubmit").on("click", function(e) {
    e.preventDefault();
    $(".searchForm").submit();

});

</script>
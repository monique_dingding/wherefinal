<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Client extends CI_Controller {

  public $username = "";
  public $data = array();
  function __construct() {
        parent::__construct();
        $this->username = "";
        $this->user_id = "";
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        if($this->session->userdata('logged_in'))
       {
          $session_data = $this->session->userdata('logged_in');
          $data['username'] = $session_data['username'];
          $data['user_id'] = $session_data['user_id'];
          $this->username = $session_data['username'];
          $this->user_id = $session_data['user_id'];
          if($session_data['role_id']!=1)
            redirect('login', 'refresh');
       }
       else
       {
         //If no session, redirect to login page
         redirect('login', 'refresh');
       }
    }

  function process_output($output = NULL) {
    $data['is_crud_table'] = TRUE;
    $data['output'] = $output;
    $this->load->view('admin2/crud_table_template', $data);
  }


 function index()
 {
      try {
        $crud = new grocery_CRUD();
        $crud->set_primary_key('id');
        $crud->set_theme('flexigrid');
        $crud->set_table('sites');
        $crud->set_subject('Establishments');

        $crud->set_relation_n_n('option', 'sites_options', 'options', 'site_id', 'option_id', "option_name");
        $crud->set_relation('city_id','cities','city_name');
        $crud->set_relation('province_id','provinces','province_name');
        $crud->set_relation('region_id','regions','region_name');
        $crud->set_relation('zipcode_id','zip_codes','code');

        $crud->set_relation('approved_by','users','username');
        $crud->add_fields('user_id','name','owner','common_term','street','city_id','province_id','region_id','zipcode_id','short_summary', 'description', 'image', 'logo','sub_image1','sub_image2','sub_image3','sub_image4','sub_image5','cover_image', 'friendly_url', 'price', 'booking_details', 'cards_accepted', 'wifi_ready', 'working_hours', 'email', 'telephone_number', 'cellphone_number', 'website', 'map', 'option');
        $crud->edit_fields('name','owner','common_term','street','city_id','province_id','region_id','zipcode_id','short_summary', 'description', 'image','logo','sub_image1','sub_image2','sub_image3','sub_image4','sub_image5', 'cover_image', 'friendly_url', 'price', 'booking_details', 'cards_accepted', 'wifi_ready', 'working_hours', 'email', 'telephone_number', 'cellphone_number', 'website', 'map', 'option');
        
        $crud->set_field_upload('image', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('cover_image', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('logo', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('sub_image1', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('sub_image2', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('sub_image3', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('sub_image4', 'assets/images/img', 'png|jpg|jpeg');
        $crud->set_field_upload('sub_image5', 'assets/images/img', 'png|jpg|jpeg');
        $crud->required_fields('name','common_term','street','city_id','province_id','region_id','zipcode_id','short_summary', 'description', 'image', 'logo','sub_image1','sub_image2','sub_image3','sub_image4','sub_image5','cover_image','owner','cellphone_number','email');
        $crud->columns('id', 'name', 'featured', 'owner', 'cellphone_number','status', 'approved_by' ,'email');
        $crud->field_type('user_id', 'hidden', $this->user_id);
        $crud->change_field_type('friendly_url','invisible');
        $crud->callback_before_insert(array($this,'generate_friendly_url'));
        $crud->display_as('approved_by','Approved By');
        $crud->unset_texteditor('map','address','description','booking_details','street');
        $crud->unset_jquery();
        $crud->unset_jquery_ui();
        
        $crud->where('sites.user_id =', $this->user_id);
        $output = $crud->render();
        $data['username'] = $this->username;
        $output->data=$data;
        $this->process_output($output);
      } catch(Exception $e){ 
        show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }   
 }

  function generate_friendly_url($post_array){
    $post_array['friendly_url'] = str_replace(' ', '-', $post_array['name']);
      return $post_array;
  }

  function articles() {

    try {
      $crud = new grocery_CRUD();

      $crud->set_theme('flexigrid');
      $crud->set_primary_key('id');
      $crud->set_table('articles');
      $crud->set_subject('Articles');
      $crud->set_relation('approved_by','users','username');
      $crud->add_fields('user_id', 'author', 'subject', 'body', 'tags', 'main_image','secondary_image', 'link');
      $crud->edit_fields('author', 'subject', 'body', 'tags', 'main_image', 'secondary_image', 'link');

      $crud->set_field_upload('main_image', 'assets/images/img', 'png|jpg|jpeg');
      $crud->set_field_upload('secondary_image', 'assets/images/img', 'png|jpg|jpeg');
      $crud->field_type('user_id', 'hidden', $this->user_id);
      $crud->required_fields('author', 'subject', 'body', 'tags', 'main_image', 'secondary_image');
      $crud->display_as('subject','Title');
      $crud->display_as('approved_by','Approved By');
      $crud->change_field_type('link','invisible');
      $crud->callback_before_insert(array($this,'generate_link'));

      // $crud->unset_texteditor('map');
      $crud->unset_texteditor('body');
      $crud->unset_jquery();
      $crud->unset_jquery_ui();
      $crud->where('articles.user_id =', $this->user_id);
      $output = $crud->render();
      $data['username'] = $this->username;
      $output->data=$data;
      $this->process_output($output);

    } catch(Exception $e){ 
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }

  }

  function generate_link($post_array){
    $post_array['link'] = str_replace(' ', '-', $post_array['subject']);
      return $post_array;
  }


public function user($id=NULL){
    $this->load->model('users_model');
    $this->load->helper(array('form', 'url', 'security'));
    $this->load->library('form_validation');
    $data['username'] = $this->username;
    $data['title'] = "User's Information";
    $data['small_title'] = "Update";
    if( $id == NULL ) $id = $this->user_id;
    $data['user'] = $this->users_model->get_user($id);
    $this->load->view('admin2/user_profile', $data);
  }
  public function insert_user($id=NULL) {
    $this->load->model('users_model');
    $this->load->helper(array('form', 'url', 'security'));
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('', '');
    $this->form_validation->set_rules('fname', 'First Name', 'required|xss_clean');
    $this->form_validation->set_rules('lname', 'Last Name', 'required|xss_clean');

    if($id!=NULL) {
      $this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
      $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|xss_clean');
      $this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|xss_clean');
    }else {
      $this->form_validation->set_rules('pwd', 'Password', 'required|xss_clean|md5');
      $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|xss_clean');
      $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
      $this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|is_unique[users.cel-num]|xss_clean');
    }

    $this->form_validation->set_rules('birthdate', 'Birthdate', 'required|xss_clean');
    
    
    if ($this->form_validation->run() == FALSE) {
      echo validation_errors();
    } else {
      $data = $this->input->post();
      if($id!=NULL){
        $this->users_model->update_user($id,$data);
        echo 'Data Updated Successfully';
      }else{
        $this->users_model->add_user($data);
        echo 'Data Inserted Successfully';
      }
    }
  }
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('admin', 'refresh');
 }

}

?>
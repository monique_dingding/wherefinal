<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcomeget_recent_articles();
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
        parent::__construct();
        $this->load->model('home_model');
    }

	public function index() {
		$data['page'] = "index";

		$data['options'] = $this->home_model->get_options();
		$data['photosliders'] = $this->home_model->get_photosliders();
		$data['articles'] = $this->home_model->get_recent_articles();
		$data['count_articles'] = $this->home_model->get_count_articles();
		$data['establishments'] = $this->home_model->get_featured_establishments();
		$data['new_locations'] = $this->home_model->get_recent_places(6, NULL, "no");

		// echo "<pre>";
		// print_r($data['photosliders']);
		// exit;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/index');
		$this->load->view('frontend/footer');
	}

	public function featured() {

		$data['page'] = "featured";
		$data['options'] = $this->home_model->get_options();
		$data['featured_establishments'] = $this->home_model->get_featured_establishments();
		$data['featured_articles'] = $this->home_model->get_featured_articles();

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/featured');
		$this->load->view('frontend/footer');
	}

	public function destinations() {

		$data['page'] = "destinations";
		$data['options'] = $this->home_model->get_options();
		$data['establishments'] = $this->home_model->get_regular_establishments();

		// echo "<pre>";
		// print_r($data['establishments']);
		// exit;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/destinations');
		$this->load->view('frontend/footer');
	}

	public function stories() {

		$data['page'] = "stories";
		$data['options'] = $this->home_model->get_options();
		if($this->uri->segment(2) !== null)
			$data['articles'] = $this->home_model->get_recent_articles(7,$this->uri->segment(2));
		else
			$data['articles'] = $this->home_model->get_recent_articles(7);

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/stories');
		$this->load->view('frontend/footer');
	}

	public function article() {
		$data['page'] = "stories";
		$data['options'] = $this->home_model->get_options();
		$data['query'] = $this->home_model->get_article($this->uri->segment(2));
		$data['articles'] = $this->home_model->get_recent_articles(5);

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/article', $data);
		$this->load->view('frontend/footer');
	}

	public function articles(){
        $page =  $_GET['page'];
        $articles = $this->home_model->getArticles($page,'');
        $count = 0;
        if(empty($articles)){
            echo 'empty';
        }else{
	        foreach($articles as $article){
	            echo '<div class="panel panel-default">
	                <div class="panel-body">
	                    <a href="'.base_url().'article/'.$article->link.'">
	                        <div class="col-md-4" style="padding: 0px;">
	                            <img src="'.base_url().'assets/images/img/'.$article->main_image.'" style="width: 100%; height: 160px; overflow: hidden;" alt="destination 5" >
	                        </div>
							<div class="col-md-8">
	                        <div class="timeline-heading">';
	            if($count%2==0)
	                echo '<h3 class="new-title">'.$article->subject.'</h3>';
	            else
	                echo '<h3 class="new-title blue">'.$article->subject.'</h3>';
	                                            
	            echo '<p class="desc text-muted" style="font-size: 14px !important;">
		            <i class="glyphicon glyphicon-time"></i>'.date("M d, Y h:iA", strtotime($article->created_at)).nbs(6).'<i class="fa fa-user"></i>'.$article->author.'</p>
		            </div><div class="timeline-body"><p>';
	            $str = rtrim(word_limiter($article->body, 50)); 
	            $str = strip_tags($str);
	            echo $str;                                             
	            echo '</p><br>';
	            $tags = explode(",", $article->tags);
				foreach($tags as $t) {
	                $t = trim($t);
	                echo '<a href="'.base_url().'tags/'.rawurlencode($t).'">
	                        <span class="label label-primary">'.$t.'</span>
	                    </a>';
	            }
	            echo '</div></div></a></div></div>';
	            $count++;
	        }
	    }
        exit;
    }

    public function featured_articles(){
    	$page =  $_GET['page'];
        $articles = $this->home_model->getFeaturedArticles($page);
        $count = 0;
        if(empty($articles)){
            echo 'empty';
        }else{
        	foreach($articles as $article){
        		echo '<div class="col-md-3" style="height:400px;">
                        <div class="widget-item">
                            <a href="'.base_url().'article/'.$article->link.'">
                                <div class="sample-thumb">
                                    <img class="featured-crop" src="'.base_url().'assets/images/img/'.$article->main_image.'" alt="Special Eve" title="Special Eve">
                                </div>
                                <h4 class="consult-title">'.$article->subject.'</h4>
                            </a>
                            <p>';
                $str = rtrim(word_limiter($article->body, 30)); 
                $str = strip_tags($str);
                echo $str; 
                echo nbs(6).'<a href="'.base_url().'article/'.$article->link.'">Read more...</a></p></div></div>';
        	}
        }
        exit;
    }

    public function establishments(){
    	$page =  $_GET['page'];
        $establishments = $this->home_model->getEstablishments($page);
        $count = 0;
        if(empty($establishments)){
            echo 'empty';
        }else{
        	foreach($establishments as $establishment){
        		echo '<div class="col-md-3">
        		    <div class="widget-item">
        		        <br>
        		        <a href="'.base_url().'place/'.$establishment->friendly_url.'">
        		            <h3 class="widget-title">'.$establishment->name.'<br><small><i class="fa fa-map-marker"></i>'.$establishment->street.', '.$establishment->city_name.', <br>'.nbs(5).$establishment->province_name.', Philippines</small></h3>
        		            <div class="sample-thumb">
        		                <img class="featured-crop" src="'.base_url().'assets/images/img/'.$establishment->image.'" alt="'.$establishment->name.'" title="'.$establishment->name.'">
        		            </div>
        		         </a>
        		            <h4 class="consult-title">'.$establishment->short_summary.'</h4>
        		            <p>'; 
        		$str = rtrim(word_limiter($establishment->description, 20)); 
        		$str = strip_tags($str);
        		echo $str;
        		echo '<br><a href="'.base_url().'place/'.$establishment->friendly_url.'" class="bluey">Read more</a></p></div></div>'; 
        	}
        }
        exit;
    }

    function stories_(){
    	$page =  $_GET['page'];
    	$tag =  $_GET['tag'];
        $stories = $this->home_model->getArticles($page,$tag);
        $count = 0;
        if(empty($stories)){
            echo 'empty';
        }else{
        	foreach($stories as $story){
        		echo '<div class="col-md-10">
        		    <div class="panel panel-default">
        		      <div class="panel-body">
        		            <div class="col-md-3" style="padding: 0px;">
        		                <img src="'.base_url().'assets/images/img/'.$story->main_image.'" class="article-crop" alt="destination 5" >
        		            </div>
        		            <div class="col-md-9">
        		                <div class="timeline-heading">
        		                 <a href="'.base_url().'article/'.$story->link.'"><h3 class="new-title">'.$story->subject.'</h3></a>
        		                  <p class="desc"><small class="text-muted"> <i class="glyphicon glyphicon-time"></i>'.date('M d, Y h:iA', strtotime($story->created_at)).nbs(5).'<i class="fa fa-user"></i>'.nbs(2).$story->author.'</small></p>
        		                </div>
        		                <div class="timeline-body">
        		                  <p>';
        		$str = rtrim(word_limiter($story->body, 50)); 
        		$str = strip_tags($str);
        		echo $str;                                              
        		echo '</p></div><br>';
        		$tags = explode(",", $story->tags);
				foreach($tags as $t) {
        		    $t = trim($t);
        		    echo '<a href="'.base_url().'tags/'.rawurlencode($t).'"><span class="label label-primary">'.$t.'</span></a> ';
        		}
        		echo '</div></div></div></div>';
        	}
        }
        exit;
    }

	public function place() {
		$data['page'] = "destinations";
		$data['options'] = $this->home_model->get_options();
		$data['query'] = $this->home_model->get_place($this->uri->segment(2));
		$data['places'] = $this->home_model->get_recent_places(5, $this->uri->segment(2));

		// echo "<pre>";
		// print_r($data['query']);
		// exit;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/place', $data);
		$this->load->view('frontend/footer');
	}


	public function subsearch() {
		redirect(base_url().'search/'.$this->input->post('option_name'));
	}

	public function search() {
		$item = urldecode($this->uri->segment(2));
		$data['page'] = "destinations";
		$data['options'] = $this->home_model->get_options();
		$data['list'] = $this->home_model->search_est_by_option($item);
		$data['option_details'] = $this->home_model->get_option_by_id($item);

		// echo "<pre>";
		// print_r($data['option_details']);
		// exit;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/search', $data);
		$this->load->view('frontend/footer');
	}

	public function create_page() {
		$data['page'] = "create-page";
		$data['options'] = $this->home_model->get_options();

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/createpage', $data);
		$this->load->view('frontend/footer');
	}

	public function submit_client() {
		$this->load->library('encrypt');

		$newdata = $this->input->post();
		$newdata['password'] = $this->encrypt->encode($newdata['password']);

		$this->home_model->create_client($newdata);

		echo "true";
	}

	public function signup_client() {
		$data['page'] = "destinations";
		$data['options'] = $this->home_model->get_options();
		$data['list'] = $this->home_model->search_est_by_option($this->uri->segment(2));
		$data['option_details'] = $this->home_model->get_option_by_id($this->uri->segment(2));

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/search', $data);
		$this->load->view('frontend/footer');
	}

	public function about_us() {
		$data['page'] = "index";
		$data['options'] = $this->home_model->get_options();
		$data['about'] = $this->home_model->get_site_settings("about");

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/about', $data);
		$this->load->view('frontend/footer');
	}

	public function contact() {
		$data['page'] = "index";
		$data['options'] = $this->home_model->get_options();
		$data['about'] = $this->home_model->get_site_settings("contact_us");

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/about', $data);
		$this->load->view('frontend/footer');

	}
}

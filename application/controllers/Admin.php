<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public $username = "";
	public $data = array();
	function __construct() {
        parent::__construct();
        $this->username = "";
        $this->user_id = "";
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
       	if($this->session->userdata('logged_in'))
		   {
		      $session_data = $this->session->userdata('logged_in');
		      $data['username'] = $session_data['username'];
		      $data['user_id'] = $session_data['user_id'];
		      $this->username = $session_data['username'];
		      $this->user_id = $session_data['user_id'];
		      if($session_data['role_id']!=2)
		        redirect('login', 'refresh');
		   }
		   else
		   {
		     //If no session, redirect to login page
		     redirect('login', 'refresh');
		   }
    }

    function process_output($output = NULL) {

    	$data['is_crud_table'] = TRUE;
    	$data['output'] = $output;
    	$this->load->view('admin2/crud_table_template', $data);
    
    }

	public function index()
	{
	  try {
			$crud = new grocery_CRUD();
			$crud->set_primary_key('id');
			$crud->set_theme('flexigrid');
			$crud->set_table('sites');
			
			$crud->set_subject('Establishments');
			
			
			$crud->set_relation_n_n('option', 'sites_options', 'options', 'site_id', 'option_id', "option_name");
			$crud->set_relation('city_id','cities','city_name');
        	$crud->set_relation('province_id','provinces','province_name');
        	$crud->set_relation('region_id','regions','region_name');
        	$crud->set_relation('zipcode_id','zip_codes','code');

			$crud->set_relation('user_id','users','username');
			$crud->add_fields('user_id','featured','friendly_url','name','owner','common_term','street','city_id','province_id','region_id','zipcode_id','short_summary', 'description', 'image', 'logo','sub_image1','sub_image2','sub_image3','sub_image4','sub_image5','cover_image', 'price', 'booking_details', 'cards_accepted', 'wifi_ready', 'working_hours', 'email', 'telephone_number', 'cellphone_number', 'website', 'map', 'option','status');
			$crud->edit_fields('user_id','featured','name','owner','common_term','street','city_id','province_id','region_id','zipcode_id','short_summary', 'description', 'image', 'logo','sub_image1','sub_image2','sub_image3','sub_image4','sub_image5','cover_image', 'price', 'booking_details', 'cards_accepted', 'wifi_ready', 'working_hours', 'email', 'telephone_number', 'cellphone_number', 'website', 'map', 'option','status');

			$crud->set_field_upload('image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('cover_image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('logo', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('sub_image1', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('sub_image2', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('sub_image3', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('sub_image4', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('sub_image5', 'assets/images/img', 'png|jpg|jpeg');
			$crud->required_fields('user_id','name','common_term','street','city_id','province_id','region_id','zipcode_id','short_summary', 'description', 'image', 'logo','sub_image1','sub_image2','sub_image3','sub_image4','sub_image5','cover_image','featured','status','owner','cellphone_number','email');
			$crud->columns('name', 'featured', 'status','user_id', 'email');
			$crud->display_as('user_id','Client');
			$crud->change_field_type('friendly_url','invisible');
			//$crud->field_type('user_id', 'hidden', $this->user_id);
			$crud->unset_texteditor('map','description','booking_details','street');
			$crud->callback_before_insert(array($this,'generate_friendly_url'));
			$crud->callback_after_update(array($this, 'approved_featured_establishment'));
			$crud->unset_jquery();
			$crud->unset_jquery_ui();

			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function generate_friendly_url($post_array){
		$post_array['friendly_url'] = str_replace(' ', '-', $post_array['name']);
  		return $post_array;
	}

	function approved_featured_establishment($post_array,$primary_key)
	{
	    $this->load->model('admin_model');
	 	
	 	if($post_array['status']=='Approved'){
		    // the message
			$msg = "Welcome! your establishment has been approved!\n";
			$subject = "Where -- email notification";

			// use wordwrap() if lines are longer than 70 characters
			$msg = wordwrap($msg,70);

			$email_ad = $this->admin_model->get_specific_info($post_array['user_id'],'email');
			// send email
			mail($email_ad ,$subject,$msg);
			$establishment_logs_update = array(
		        "id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s'),
		        "approved_by" => $this->user_id
	    	);
		}else if($post_array['featured']=='Yes'){
		    // the message
			$msg = "Welcome! your establishment has been featured!\n";
			$subject = "Where -- email notification";

			// use wordwrap() if lines are longer than 70 characters
			$msg = wordwrap($msg,70);

			$email_ad = $this->admin_model->get_specific_info($post_array['user_id'],'email');
			// send email
			mail($email_ad ,$subject,$msg);
			$establishment_logs_update = array(
		        "id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s'),
		        "approved_by" => $this->user_id
	    	);
		}else{
			$establishment_logs_update = array(
		        "id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s')
		    );
		}
	    $this->db->update('sites',$establishment_logs_update,array('id' => $primary_key));
	 
	    return true;
	}
	function users() {

		try {
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_primary_key('user_id');
			$crud->set_table('users');
			$crud->set_subject('Users');
			$crud->set_relation('role_id','roles','description');
			$crud->set_relation('approved_by','users','username');
			$crud->add_fields('fname', 'lname', 'username', 'email', 'pwd', 'cel-num', 'tel-num', 'birthdate',  'role_id', 'status');
			$crud->edit_fields('fname', 'lname', 'username', 'email', 'pwd', 'cel-num', 'tel-num', 'birthdate', 'role_id', 'status');
			$crud->columns('fname', 'lname', 'username', 'email','cel-num', 'tel-num', 'birthdate', 'role_id', 'status' ,'approved_by');
			$crud->display_as('role_id','role');
			$crud->display_as('approved_by','Approved By');
			//$crud->field_type('status','dropdown', array('1' => 'Approved', '0' => 'Pending'));
			//$crud->field_type('role_id','dropdown', array('1'=>'Client','2'=>'Admin','3' =>'Super Admin'));
			//$crud->change_field_type('pwd','password');
			$crud->callback_before_insert(array($this,'encrypt_password_callback'));
			$crud->callback_before_update(array($this,'encrypt_password_callback'));
			$crud->callback_edit_field('pwd',array($this,'decrypt_password_callback'));
			$crud->display_as('fname','First Name')->display_as('lname','Last Name')->display_as('pwd','Password')->display_as('role_id','Role');
			$crud->required_fields('fname', 'lname', 'username', 'email', 'pwd', 'cel-num', 'tel-num', 'birthdate', 'role_id', 'status');
			$crud->callback_after_update(array($this, 'approved_user_notification'));
			// $crud->unset_texteditor('map');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$crud->where('users.user_id !=', $this->user_id);
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	function approved_user_notification($post_array,$primary_key)
	{
	    
	 	
	 	if($post_array['status']=='Approved'){
		    // the message
			$msg = "Welcome! your account has been approved!\n";
			$subject = "Where -- email notification";

			// use wordwrap() if lines are longer than 70 characters
			$msg = wordwrap($msg,70);

			// send email
			mail($post_array['email'],$subject,$msg);
			$user_logs_update = array(
		        "user_id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s'),
		        "approved_by" => $this->user_id
	    	);
		}else{
			$user_logs_update = array(
		        "user_id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s')
		    );
		}
	    $this->db->update('users',$user_logs_update,array('user_id' => $primary_key));
	 
	    return true;
	}

	function encrypt_password_callback($post_array, $primary_key = null){
		$this->load->library('encrypt');
		$post_array['pwd'] = $this->encrypt->encode($post_array['pwd']);
		return $post_array;
	}

	function decrypt_password_callback($value){
		$this->load->library('encrypt');
		$decrypted_password = $this->encrypt->decode($value);
		return "<input type='text' name='pwd' value='$decrypted_password' class='form-control' />";
	}

	function articles() {

		try {
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_primary_key('id');
			$crud->set_table('articles');
			$crud->set_subject('Articles');
			$crud->set_relation('user_id','users','username');
			$crud->set_relation('approved_by','users','username');
			$crud->add_fields( 'user_id','author', 'subject', 'body', 'tags', 'main_image', 'secondary_image', 'link','approved','featured');
			$crud->edit_fields('user_id','author', 'subject', 'body', 'tags', 'main_image', 'secondary_image','approved','featured');

			$crud->set_field_upload('main_image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('secondary_image', 'assets/images/img', 'png|jpg|jpeg');
			// $crud->unset_texteditor('map');
			//$crud->field_type('user_id', 'hidden', $this->user_id);
			//$crud->field_type('user_id', 'hidden', $this->user_id);
			$crud->display_as('user_id','client');
			$crud->display_as('approved_by','Approved By');
			$crud->display_as('subject','title');
			$crud->change_field_type('link','invisible');
			$crud->unset_texteditor('body');
			$crud->callback_before_insert(array($this,'generate_link'));
			$crud->callback_after_update(array($this, 'approved_featured_article'));
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	function generate_link($post_array){
		$post_array['link'] = str_replace(' ', '-', $post_array['subject']);
  		return $post_array;
	}

	function approved_featured_article($post_array,$primary_key)
	{
	    $this->load->model('admin_model');
	 	
	 	if($post_array['approved']=='Approved'){
		    // the message
			$msg = "Welcome! your article has been approved!\n";
			$subject = "Where -- email notification";

			// use wordwrap() if lines are longer than 70 characters
			$msg = wordwrap($msg,70);

			$email_ad = $this->admin_model->get_specific_info($post_array['user_id'],'email');
			// send email
			mail($email_ad ,$subject,$msg);
			$establishment_logs_update = array(
		        "id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s'),
		        "approved_by" => $this->user_id
	    	);
		}else if($post_array['featured']=='Yes'){
		    // the message
			$msg = "Welcome! your article has been featured!\n";
			$subject = "Where -- email notification";

			// use wordwrap() if lines are longer than 70 characters
			$msg = wordwrap($msg,70);

			$email_ad = $this->admin_model->get_specific_info($post_array['user_id'],'email');
			// send email
			mail($email_ad ,$subject,$msg);
			$establishment_logs_update = array(
		        "id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s'),
		        "approved_by" => $this->user_id
	    	);
		}else{
			$establishment_logs_update = array(
		        "id" => $primary_key,
		        "updated_at" => date('Y-m-d H:i:s')
		    );
		}
	    $this->db->update('articles',$establishment_logs_update,array('id' => $primary_key));
	 
	    return true;
	}

	function photos() {

		try {
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_primary_key('id');
			$crud->set_table('photos_slider');
			$crud->set_subject('Photos Slider');
			$crud->add_fields('site_id', 'image', 'thumbnail');
			$crud->edit_fields('site_id', 'image', 'thumbnail');
			$crud->set_relation('site_id', 'sites', 'name');
			$crud->display_as('site_id', 'Establishment');
			$crud->set_field_upload('image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('thumbnail', 'assets/images/img', 'png|jpg|jpeg');
			// $crud->unset_texteditor('map');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function options(){
		try {
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('options');
			$crud->set_subject('Options');
			$crud->add_fields('option_name', 'option_image', 'option_statement');
			$crud->edit_fields('option_name', 'option_image', 'option_statement');
			$crud->required_fields('option_name', 'option_image', 'option_statement');
			$crud->set_field_upload('option_image', 'assets/images', 'png|jpg|jpeg');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function cities(){
		try {
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('cities');
			$crud->set_subject('Cities');
			$crud->add_fields('city_name');
			$crud->edit_fields('city_name');
			$crud->required_fields('city_name');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function provinces(){
		try {
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('provinces');
			$crud->set_subject('Provinces');
			$crud->add_fields('province_name');
			$crud->edit_fields('province_name');
			$crud->required_fields('province_name');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function zipcodes(){
		try {
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('zip_codes');
			$crud->set_subject('Zip Codes');
			$crud->add_fields('code');
			$crud->edit_fields('code');
			$crud->required_fields('code');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function logout()
	 {
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('admin', 'refresh');
	 }

	 function settings(){
	 	try {
	 		$crud = new grocery_CRUD();
	 		$crud->set_theme('flexigrid');
	 		$crud->set_table('sites_settings');
	 		$crud->set_subject('Site Settings');
	 		$crud->edit_fields('content');
	 		$crud->add_fields('content');
	 		$crud->required_fields('content');
	 		$crud->unset_add();
	 		$crud->unset_delete();
	 		$crud->unset_jquery();
	 		$crud->unset_jquery_ui();
	 		$output = $crud->render();
	 		$data['username'] = $this->username;
	 		$output->data=$data;
	 		$this->process_output($output);

	 	} catch(Exception $e){ 
	 		show_error($e->getMessage().' --- '.$e->getTraceAsString());
	 	}	
	 }

	function sponsors(){
		try {
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('sponsors');
			$crud->set_subject('Sponsors');
			$crud->add_fields('filename','desc');
			$crud->edit_fields('filename','desc');
			$crud->required_fields('filename','desc');
			$crud->set_field_upload('filename', 'assets/images', 'png|jpg|jpeg');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
			$data['username'] = $this->username;
			$output->data=$data;
			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
/*
	public function users() {
		$this->load->model('users_model');
		$data['title'] = "Users";
		$data['small_title'] = "Manage list";
		$data['username'] = $this->username;
		$data['users'] = $this->users_model->get_users();
		$this->load->view('admin/users', $data);
	}
*/
	public function user($id=NULL){
		$this->load->model('users_model');
		$this->load->helper(array('form', 'url', 'security'));
		$this->load->library('form_validation');
		$data['username'] = $this->username;
		$data['title'] = "User's Information";
		$data['small_title'] = "Update";
		if( $id == NULL ) $id = $this->user_id;
		$data['user'] = $this->users_model->get_user($id);
		$this->load->view('admin2/user_profile', $data);
	}

	public function delete_user($id=NULL)
	{
	   $this->load->model('users_model');
	   $this->users_model->delete_row($id, 'user_id','users');
	   redirect($_SERVER['HTTP_REFERER']);  
	}


	public function insert_user($id=NULL) {
		$this->load->model('users_model');
		$this->load->helper(array('form', 'url', 'security'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('lname', 'Last Name', 'required|xss_clean');

		if($id!=NULL) {
			$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
			$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|xss_clean');
			$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|xss_clean');
		}else {
			$this->form_validation->set_rules('pwd', 'Password', 'required|xss_clean|md5');
			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|xss_clean');
			$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
			$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|is_unique[users.cel-num]|xss_clean');
		}

		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		} else {
			$data = $this->input->post();
			if($id!=NULL){
				$this->users_model->update_user($id,$data);
				echo 'Data Updated Successfully';
			}else{
				$this->users_model->add_user($data);
				echo 'Data Inserted Successfully';
			}
		}
	}

}
